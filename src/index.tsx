import React, { ReactElement } from "react";

import { AppBootstrap } from "@components";

import { Navigator } from "@config";
import { SettingsProvider } from "@contexts/settings-context";

export default function App(): ReactElement {
    return (
        <SettingsProvider>
            <AppBootstrap>

                <Navigator />
            </AppBootstrap>
        </SettingsProvider>
    );
}
