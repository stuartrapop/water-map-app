import AsyncStorage from "@react-native-async-storage/async-storage";

import { DrawNavigatorParams } from "@config";
import { DrawerNavigationProp } from "@react-navigation/drawer";
import { DrawerActions } from "@react-navigation/native";
import { mapStateType, MarkerPointType, updateMapDataAndMarkers } from "@utils";
import * as Location from "expo-location";
import React, { ReactElement, useEffect, useRef, useState } from "react";
import { Dimensions, Pressable, SafeAreaView, Text, View } from "react-native";
import MapView from "react-native-map-clustering";
import { Marker, Region } from "react-native-maps";

import {
    Copyright,
    GooglePlacesInput,
    Header,
    LocationButton,
    MapZoomPanel,
    Popup
} from "@components";

import { useTranslation } from "react-i18next";
import { ServerUrl } from "../../utils/envConstants";
import styles from "./home.styles";

type MapProps = {
    navigation: DrawerNavigationProp<DrawNavigatorParams, "Home">;
};

const getRegionForZoom = (lat: number, lon: number, zoom: number) => {
    const distanceDelta = Math.exp(Math.log(360) - zoom * Math.LN2);
    const { width, height } = Dimensions.get("screen");
    const aspectRatio = width / height;
    return {
        latitude: lat,
        longitude: lon,
        latitudeDelta: distanceDelta * aspectRatio,
        longitudeDelta: distanceDelta,
    };
};

const Map = ({ navigation }: MapProps): ReactElement => {
    const { t } = useTranslation();
    const toggleDrawer = () => {
        navigation.dispatch(DrawerActions.toggleDrawer());
    };
    const navigateHome = () => {
        navigation.navigate("Home");
    };
    const [showPopup, setShowPopup] = useState(false);
    const closePopup = () => setShowPopup(false);
    const [zooming, setZooming] = useState(false);
    const [markers, setMarkers] = useState<MarkerPointType[]>([]);
    const [serverImageIds, setServerImageIds] = useState<string[]>([]);
    const getServerImageIds = async () => {
        try {
            const response = await fetch(
                `${ServerUrl}/availablePhotos??${Date.now()}`
            );
            const json = await response.json();

            setServerImageIds(json.osm_ids);
        } catch (error) {
            console.log(error);
        }
    };

    const [mapState, setMapState] = useState<mapStateType>({
        jsonData: [],
        bbox: "",
        zoom: 18,
        showConnectionError: false,
        popupProps: {
            name: "",
            id_osm: 1,
            image: "",
            mapillary: "",
            ewp: "",
            latitude: 4,
            longitude: 6,
            closePopup,
            hasServerImage: false,
            navigation,
        },
        region: {
            latitude: 48.8566,
            longitude: 2.3522,
            latitudeDelta: 1,
            longitudeDelta: 1,
        },
    });

    const { zoom, showConnectionError, popupProps, region } = mapState;

    React.useEffect(() => {
        const unsubscribe = navigation.addListener("focus", async () => {
            setTimeout(() => {
                getServerImageIds();
            }, 300);
        });

        return unsubscribe;
    }, [navigation]);
    React.useEffect(() => {
        getServerImageIds();
    }, []);

    const mapRef = useRef(null);
    useEffect(() => {
        let mounted = true;
        (async () => {
            try {
                const { status } =
                    await Location.requestForegroundPermissionsAsync();
                if (status !== "granted") {
                    console.log("Permission to access location was denied");
                    return;
                } else {
                    console.log("Permission granted for location");
                }

                const location = await Location.getCurrentPositionAsync({});
                if (mounted) {
                    const regionResponse = await AsyncStorage.getItem(
                        "@region"
                    );
                    let regionJson: Region | null;
                    if (regionResponse) {
                        regionJson = JSON.parse(regionResponse) as Region;

                        await updateMapDataAndMarkers(
                            mapState,
                            setMapState,
                            setMarkers,
                            regionJson
                        );


                        mapRef.current.animateToRegion(
                            {
                                ...regionJson,
                            },
                            100
                        );

                    } else {
                        setMapState({
                            ...mapState,
                            region: {
                                ...region,
                                latitude: location.coords.latitude,
                                longitude: location.coords.longitude,
                            },
                        });
                    }
                }
            } catch (error) {
                console.log(error);
            }
        })();

        return function cleanup() {
            mounted = false;
        };
    }, []);

    const gotoCurrentPosition = () => {
        Location.getCurrentPositionAsync({})
            .then((position) => {
                // @ts-expect-error : useRef typing not working
                mapRef.current.animateToRegion(
                    {
                        ...region,
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                    },
                    1500
                );
            })
            .catch((error) => {
                console.log(error);
            });
    };

    const mapZoomIn = () => {
        if (zooming) return;
        setZooming(true);
        if (zoom >= 19) {
            setMapState({ ...mapState, zoom: 19 });
        } else {
            setMapState({ ...mapState, zoom: zoom + 1 });
            const regn = getRegionForZoom(
                region.latitude,
                region.longitude,
                zoom + 1
            );
            // @ts-expect-error : useRef typing not working
            mapRef.current.animateToRegion(regn, 50);
        }
        setZooming(false);
    };

    const mapZoomOut = () => {
        if (zooming) return;
        setZooming(true);
        if (zoom <= 10) {
            setMapState({ ...mapState, zoom: 10 });
        } else {
            setMapState({ ...mapState, zoom: zoom - 1 });
            const regn = getRegionForZoom(
                region.latitude,
                region.longitude,
                zoom - 1
            );
            // @ts-expect-error : useRef typing not working
            mapRef.current.animateToRegion(regn, 50);
        }
        setZooming(false);
    };

    const onRegionChangeComplete = async (newRegion: Region) => {
        await updateMapDataAndMarkers(
            mapState,
            setMapState,
            setMarkers,
            newRegion
        );
    };

    console.log("home page rerendering", region);
    return (
        <SafeAreaView style={styles.container}>
            <Header toggleDrawer={toggleDrawer} navigateHome={navigateHome} disactivateHome={true} />

            <View style={{ flex: 9 }}>
                <GooglePlacesInput
                    region={region}
                    mapState={mapState}
                    setMapState={setMapState}
                    mapRef={mapRef}
                />
                <Pressable style={styles.map}>
                    {/* Pressable suggested on github 3897 */}
                    <MapView
                        // clusterColor="orange"
                        clusterColor="#79C8FB"
                        clusteringEnabled={true}
                        initialRegion={region}
                        minPoints={4}
                        radius={60}
                        minZoomLevel={10} // default => 0
                        maxZoomLevel={19} // default => 20
                        moveOnMarkerPress={false}
                        onRegionChangeComplete={onRegionChangeComplete}
                        provider="google"
                        ref={mapRef}
                        pitchEnabled={false}
                        rotateEnabled={false}
                        showsMyLocationButton={false}
                        showsUserLocation={true}
                        style={styles.map}
                        toolbarEnabled={false}
                        tracksViewChanges={false}
                        zoomTapEnabled={false}
                    >
                        {markers.map((item) => {
                            const hasServerImage =
                                serverImageIds.indexOf(
                                    item.id_osm.toString()
                                ) != -1;

                            const hasImage =
                                item.image ||

                                item.ewp ||
                                hasServerImage;

                            const isSelectedPinOnOpenPopup = (item.id_osm === popupProps.id_osm) && showPopup;

                            const pinColor = isSelectedPinOnOpenPopup ? "green" : hasImage ? "#65447E" : "#EE5F5F";

                            return (
                                <Marker
                                    tracksViewChanges={false}
                                    key={`${item.id}-${hasImage ? "active" : "inactive"
                                        }-${isSelectedPinOnOpenPopup ? "active" : "inactive"
                                        }`}
                                    pinColor={pinColor}
                                    onPress={(e) => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        setMapState({
                                            ...mapState,
                                            popupProps: {
                                                ...popupProps,
                                                name: item.name,
                                                id_osm: item.id_osm,
                                                latitude: item.latitude,
                                                longitude: item.longitude,
                                                image: item.image,
                                                mapillary: item.mapillary,
                                                ewp: item.ewp,
                                                hasServerImage: hasServerImage,
                                            },
                                        });
                                        setShowPopup(!showPopup);
                                    }}
                                    coordinate={{
                                        latitude: item.latitude,
                                        longitude: item.longitude,
                                    }}
                                ></Marker>
                            );
                        })}
                    </MapView>
                </Pressable>
                {showConnectionError && (
                    <View
                        style={{
                            alignContent: "center",
                            position: "absolute",
                            bottom: 150,
                            width: "85%",
                            marginLeft: "7%",
                            backgroundColor: "white",
                            padding: 15,
                        }}
                    >
                        <Text
                            style={{
                                color: "red",
                                fontSize: 20,
                                fontWeight: "bold",
                            }}
                        >
                            {t("camera.dataError")}
                        </Text>
                    </View>
                )}
                <Copyright />
                {showPopup && <Popup {...popupProps} />}
                <MapZoomPanel onZoomIn={mapZoomIn} onZoomOut={mapZoomOut} />
                <LocationButton gotoCurrentPosition={gotoCurrentPosition} />
            </View>
        </SafeAreaView>
    );
};

export default Map;
