import { StyleSheet, Dimensions } from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        height: "100%",
        backgroundColor: "#fff",
    },
    map: {
        flex: 9,
    },
    searchbox: {
        position: "absolute",
        top: 10,
        borderRadius: 10,
        margin: 10,
        color: "#000",
        borderColor: "#666",
        backgroundColor: "#FFF",
        borderWidth: 1,
        height: 45,
        paddingHorizontal: 10,
        fontSize: 18,
    },
    mapContainer: {},
    customMarker: {
        width: 50,
        height: 100,
    },
});

export default styles;
