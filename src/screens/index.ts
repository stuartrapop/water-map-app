export { default as Plastic } from "./plastic/plastic";
export { default as Fountain } from "./fountain/fountain";
export { default as Home } from "./home/home";
export { default as Project } from "./project/project";
export { default as Tap } from "./tap/tap";
export { default as Bottle } from "./bottle/bottle";
export { default as CameraScreen } from "./camera/camera";
