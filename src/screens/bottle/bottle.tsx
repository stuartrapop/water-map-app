import React, { ReactElement } from "react";

import { DrawNavigatorParams } from "@config";
import { PageContainer } from "@components";
import { DrawerNavigationProp } from "@react-navigation/drawer";
import { DrawerActions } from "@react-navigation/native";
import { useTranslation } from "react-i18next";

type PlasticProps = {
    navigation: DrawerNavigationProp<DrawNavigatorParams, "Bottle">;
};

export default function Bottle({ navigation }: PlasticProps): ReactElement {
    const { t } = useTranslation();
    const toggleDrawer = () => {
        navigation.dispatch(DrawerActions.toggleDrawer());
    };
    const navigateHome = () => {
        navigation.navigate("Home");
    };

    const pageContainerProps = {
        toggleDrawer,
        navigateHome,
        title: t("bottle.title"),
        page: t("bottle.page"),
    };

    return <PageContainer {...pageContainerProps}></PageContainer>;
}
