import React, { ReactElement } from "react";

import { DrawNavigatorParams } from "@config";
import { PageContainer } from "@components";
import { DrawerNavigationProp } from "@react-navigation/drawer";
import { DrawerActions } from "@react-navigation/native";
import { useTranslation } from "react-i18next";

type PlasticProps = {
    navigation: DrawerNavigationProp<DrawNavigatorParams, "Fountain">;
};

export default function Project({ navigation }: PlasticProps): ReactElement {
    const { t } = useTranslation();
    const toggleDrawer = () => {
        navigation.dispatch(DrawerActions.toggleDrawer());
    };
    const navigateHome = () => {
        navigation.navigate("Home");
    };

    const pageContainerProps = {
        toggleDrawer,
        navigateHome,
        title: t("fountain.title"),
        page: t("fountain.page"),
    };

    return <PageContainer {...pageContainerProps}></PageContainer>;
}
