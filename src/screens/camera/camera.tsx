import React, { ReactElement, useState } from "react";
import { useTranslation } from "react-i18next";
import * as Location from "expo-location";
import * as ImageManipulator from "expo-image-manipulator";
import { MaterialIcons, Ionicons } from "@expo/vector-icons";
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Alert,
    SafeAreaView,
} from "react-native";
import { Camera, CameraCapturedPicture } from "expo-camera";
import { DrawerNavigationProp } from "@react-navigation/drawer";
import { CameraPreview, Header } from "@components";
import { DrawNavigatorParams } from "@config";
import { DrawerActions } from "@react-navigation/native";

// import Constants from "expo-constants";
// const { manifest } = Constants;
// const uri = `http://${manifest.debuggerHost.split(":").shift()}:3000`;

import { ServerUrl } from "../../utils/envConstants";
const uri = ServerUrl;

let camera: Camera | null;

type CameraStateType = {
    isLoading: boolean;
    previewVisible: boolean;
    showConnectionError: boolean;
    showConfirmPage: boolean;
    showConfirmSuccess: boolean;
    capturedImage: CameraCapturedPicture | undefined;
};

type CameraProps = {
    navigation: DrawerNavigationProp<DrawNavigatorParams, "Camera">;
    route: {
        params: {
            id_osm: number;
        };
    };
};
export default function CameraScreen({
    navigation,
    route,
}: CameraProps): ReactElement | null {
    const [cameraState, setCameraState] = useState<CameraStateType>({
        isLoading: true,
        previewVisible: false,
        showConnectionError: false,
        showConfirmPage: false,
        showConfirmSuccess: false,
        capturedImage: undefined,
    });

    const [location, setLocation] = useState<Location.LocationObject | null>(
        null
    );

    const {
        isLoading,
        previewVisible,
        showConnectionError,
        showConfirmPage,
        showConfirmSuccess,
        capturedImage,
    } = cameraState;
    const { t } = useTranslation();

    const { id_osm } = route.params;

    const __startCamera = async () => {
        setCameraState({
            ...cameraState,
            previewVisible: false,
            showConnectionError: false,
            showConfirmPage: false,
            showConfirmSuccess: false,
            capturedImage: undefined,
        });
        const { status } = await Camera.requestCameraPermissionsAsync();
        console.log(status);
        if (status === "granted") {
            console.log("granted", status);
            setCameraState({
                ...cameraState,
                isLoading: false,
            });
        } else {
            Alert.alert(t("camera.cameraPermissionError"));
            setCameraState({
                ...cameraState,
                isLoading: true,
            });
            navigation.navigate("Home");
        }
    };

    const cameraOptions = {
        quality: 0.3,
        exif: false,
        skipProccessing: true,
        scale: 1,
    };
    const __takePicture = async () => {
        try {
            const { status } =
                await Location.requestForegroundPermissionsAsync();
            if (status !== "granted") {
                console.log("Permission to access location was denied");
                return;
            } else {
                const locationResponse = await Location.getCurrentPositionAsync(
                    { accuracy: Location.Accuracy.Highest }
                );
                setLocation(locationResponse);
                console.log("Permission granted for location");
            }
        } catch (error) {
            console.log("error getting location in camera");
        }
        setCameraState({ ...cameraState, showConnectionError: false });

        const photo: CameraCapturedPicture | undefined =
            await camera?.takePictureAsync(cameraOptions);
        console.log(photo);
        setCameraState({
            ...cameraState,
            previewVisible: true,
            capturedImage: photo,
        });
    };
    const __savePhoto = async (photo: CameraCapturedPicture) => {
        const resizedUri = await ImageManipulator.manipulateAsync(photo.uri, [
            { resize: { width: 500, height: 500 } },
        ]);

        const newUri = resizedUri.uri;

        setCameraState({
            ...cameraState,
            showConnectionError: false,
            showConfirmPage: true,
        });

        const data = new FormData();
        data.append("name", `${id_osm}`);
        data.append("location", JSON.stringify(location));

        data.append("uploaded_file", {
            uri: newUri,
            name: "upload.jpg",
            type: "image/jpg",
        });

        try {
            console.log("about to send data");
            const res = await fetch(`${uri}/saveImage`, {
                method: "POST",
                body: data,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "multipart/form-data",
                },
            });
            const responseJson = await res.json();

            console.log("in save photo function", responseJson);
            if (responseJson.message === "success") {
                setCameraState({
                    ...cameraState,
                    showConfirmPage: true,
                    showConfirmSuccess: true,
                });
            } else {
                setCameraState({
                    ...cameraState,
                    showConfirmPage: true,
                    showConnectionError: true,
                });
            }
        } catch (error) {
            console.log("error camera save", error);
            setCameraState({
                ...cameraState,
                showConfirmPage: true,
                showConnectionError: true,
            });
        }
    };
    const __retakePicture = () => {
        setCameraState({
            ...cameraState,
            showConfirmPage: true,


            previewVisible: false,
            capturedImage: undefined,
        });
    };

    React.useEffect(() => {
        const unsubscribe = navigation.addListener("focus", () => {
            setCameraState({
                isLoading: true,
                previewVisible: false,
                showConnectionError: false,
                showConfirmPage: false,
                showConfirmSuccess: false,
                capturedImage: undefined,
            });

            console.log("in useEffect", cameraState);

            __startCamera();
        });

        return unsubscribe;
    }, [navigation, []]);
    // React.useEffect(() => {
    //     __startCamera();
    // }, []);
    const toggleDrawer = () => {
        navigation.dispatch(DrawerActions.toggleDrawer());
    };
    const navigateHome = () => {
        navigation.navigate("Home");
    };
    if (isLoading) return null;

    return (
        <SafeAreaView style={styles.container}>
            <Header toggleDrawer={toggleDrawer} navigateHome={navigateHome} />

            {!showConfirmPage && (
                <View
                    style={{
                        flex: 9,
                        width: "95%",
                    }}
                >
                    {previewVisible && capturedImage ? (
                        <CameraPreview
                            photo={capturedImage}
                            navigation={navigation}
                            savePhoto={() => {
                                __savePhoto(capturedImage);
                            }}
                            retakePicture={__retakePicture}
                        />
                    ) : (
                        <Camera
                            type={Camera.Constants.Type.back}
                            flashMode={"off"}
                            style={{ flex: 1, borderRadius: 10 }}
                            ref={(r) => {
                                camera = r;
                            }}
                        >
                            <View
                                style={{
                                    flex: 1,
                                    width: "100%",
                                    backgroundColor: "transparent",
                                    flexDirection: "row",
                                }}
                            >
                                <View
                                    style={{
                                        position: "absolute",
                                        left: "5%",
                                        top: "10%",
                                        flexDirection: "column",
                                        justifyContent: "space-between",
                                    }}
                                ></View>
                                <View
                                    style={{
                                        position: "absolute",
                                        bottom: 0,
                                        flexDirection: "row",
                                        flex: 1,
                                        width: "100%",
                                        padding: 20,
                                        justifyContent: "space-between",
                                    }}
                                >
                                    <View
                                        style={{
                                            alignSelf: "center",
                                            flex: 1,
                                            alignItems: "center",
                                        }}
                                    >
                                        <TouchableOpacity
                                            onPress={__takePicture}
                                            style={{
                                                width: 70,
                                                height: 70,
                                                bottom: 0,
                                                borderRadius: 50,
                                                backgroundColor: "#fff",
                                                flex: 1,
                                                alignItems: "center",
                                                justifyContent: "center",
                                            }}
                                        >
                                            <MaterialIcons
                                                name="camera"
                                                size={55}
                                                color="black"
                                            />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </Camera>
                    )}
                </View>
            )}
            {showConfirmPage && (
                <View
                    style={{
                        flex: 9,
                        width: "100%",
                        alignItems: "center",
                        justifyContent: "flex-start",
                    }}
                >
                    <Text
                        style={{
                            marginTop: 40,
                            marginBottom: 60,
                            fontSize: 24,
                            padding: 10,
                        }}
                    >
                        {t("camera.thanks")}
                    </Text>

                    <Text
                        style={{
                            marginTop: 40,
                            marginBottom: 60,
                            fontSize: 22,
                            paddingLeft: 10,
                        }}
                    >
                        {showConfirmSuccess && t("camera.success")}
                    </Text>

                    <TouchableOpacity
                        onPress={() => {
                            setCameraState({
                                ...cameraState,
                                isLoading: true,
                                previewVisible: false,
                                showConnectionError: false,
                                showConfirmPage: false,
                                showConfirmSuccess: false,
                            });

                            navigation.navigate("Home");
                        }}
                        style={{
                            flex: 1,
                            alignItems: "center",
                            justifyContent: "center",

                            position: "absolute",
                            bottom: 40,
                            width: "100%",
                        }}
                    >
                        <Ionicons
                            name="ios-checkmark-done-sharp"
                            size={50}
                            color="green"
                        />
                    </TouchableOpacity>

                    {showConnectionError && (
                        <View
                            style={{
                                position: "absolute",
                                bottom: 250,
                                backgroundColor: "white",
                                padding: 15,
                            }}
                        >
                            <Text style={{ fontSize: 24, color: "red" }}>
                                {t("camera.saveError")}
                            </Text>
                        </View>
                    )}
                </View>
            )}
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
    },
});
