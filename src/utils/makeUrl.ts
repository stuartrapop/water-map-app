type Coordinates = {
  latitude: number
  longitude: number
}

const makeUrl = ({ latitude, longitude }: Coordinates): string => {
  function pad4(num: number): string {
    let s = num + ''
    while (s.length < 4) s = '0' + s
    return s
  }

  // dimension of one grid cell
  const bbox_grid_dimension = {
    lonDim: 3,
    latDim: 3,
  }

  //figure out which cell you are in
  const grid_offset = {
    lonOff: Math.floor((longitude + 180.0) / bbox_grid_dimension.lonDim),
    latOff: Math.floor((latitude + 90.0) / bbox_grid_dimension.latDim),
  }

  //based on cell, start building the file name for that cell
  const current_bbox = {
    lonMin: -180 + grid_offset.lonOff * bbox_grid_dimension.lonDim,
    latMin: -90 + grid_offset.latOff * bbox_grid_dimension.latDim,
    lonMax: -180 + grid_offset.lonOff * bbox_grid_dimension.lonDim + bbox_grid_dimension.lonDim,
    latMax: -90 + grid_offset.latOff * bbox_grid_dimension.latDim + bbox_grid_dimension.latDim,
  }

  const filename =
    'https://water-map.org/gridFountain/fullgrid2_' +
    pad4(current_bbox.lonMin) +
    '_' +
    pad4(current_bbox.latMin) +
    '_' +
    pad4(current_bbox.lonMax) +
    '_' +
    pad4(current_bbox.latMax) +
    '.json'

  return filename
}

export default makeUrl
