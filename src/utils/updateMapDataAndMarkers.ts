import AsyncStorage from "@react-native-async-storage/async-storage";
import generateMarkers from "./generateMarkers";

import { ServerUrl } from "./envConstants";

import { mapStateType, MarkerPointType } from "@utils";
import { Region } from "react-native-maps";

const getZoomFromRegion = (region: Region) => {
    return Math.round(Math.log(360 / region.longitudeDelta) / Math.LN2);
};

const updateMapDataAndMarkers = async (
    mapState: mapStateType,
    setMapState: (newState: mapStateType) => void,
    setMarkers: (newData: MarkerPointType[]) => void,
    newRegion: Region
): Promise<void> => {
    const { jsonData, bbox } = mapState;

    const zoom = getZoomFromRegion(newRegion);

    function pad4(num: number): string {
        let s = num + "";
        while (s.length < 4) s = "0" + s;
        return s;
    }

    // dimension of one grid cell
    const bbox_grid_dimension = {
        lonDim: 3,
        latDim: 3,
    };

    //figure out which cell you are in now
    const new_grid_offset = {
        lonOff: Math.floor(
            (newRegion.longitude + 180.0) / bbox_grid_dimension.lonDim
        ),
        latOff: Math.floor(
            (newRegion.latitude + 90.0) / bbox_grid_dimension.latDim
        ),
    };

    //based on cell, start building the file name for that cell
    const current_bbox = {
        lonMin: -180 + new_grid_offset.lonOff * bbox_grid_dimension.lonDim,
        latMin: -90 + new_grid_offset.latOff * bbox_grid_dimension.latDim,
        lonMax:
            -180 +
            new_grid_offset.lonOff * bbox_grid_dimension.lonDim +
            bbox_grid_dimension.lonDim,
        latMax:
            -90 +
            new_grid_offset.latOff * bbox_grid_dimension.latDim +
            bbox_grid_dimension.latDim,
    };

    const boxString = `${pad4(current_bbox.lonMin)}_${pad4(
        current_bbox.latMin
    )}_${pad4(current_bbox.lonMax)}_${pad4(current_bbox.latMax)}`;

    const filename = `${ServerUrl}/gridFountain/fullgrid2_${boxString}.json`;
    if (bbox === boxString && jsonData.length !== 0) {
        const markersArray = generateMarkers(jsonData, newRegion);
        setMapState({
            ...mapState,
            showConnectionError: false,
            region: newRegion,
            zoom: zoom,
        });
        setMarkers(markersArray);
        console.log("number of markers", markersArray.length);
        AsyncStorage.setItem("@region", JSON.stringify(newRegion));
    } else {
        console.log("getting new data");
        console.log(newRegion);
        try {
            const response = await fetch(filename);
            const jsonResponse = await response.json();
            const jsonDataForMarkers = jsonResponse.features;
            const markersArray = generateMarkers(jsonDataForMarkers, newRegion);
            setMapState({
                ...mapState,
                showConnectionError: false,
                jsonData: jsonDataForMarkers,
                bbox: boxString,
                region: newRegion,
                zoom: zoom,
            });
            setMarkers(markersArray);
            console.log("number of markers", markersArray.length);
            AsyncStorage.setItem("@region", JSON.stringify(newRegion));
        } catch (error) {
            console.log("in error");
            setMapState({ ...mapState, showConnectionError: true, bbox: "" });
        }
    }
};

export default updateMapDataAndMarkers;
