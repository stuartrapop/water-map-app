const getWikiCommonsUser = async (url: string): Promise<string> => {
    const decodeUrl = decodeURIComponent(url);
    let imageName: string;
    if (decodeUrl.indexOf("File:") == -1) {
        imageName = decodeUrl;
    } else {
        imageName = decodeUrl.substring(
            decodeUrl.indexOf("File:") + 5,
            decodeUrl.length
        );
    }
    const queryString =
        "https://commons.wikimedia.org/w/api.php?action=query&format=json&prop=imageinfo&titles=File:" +
        imageName +
        "&iiprop=user&meta=siteinfo&siprop=rightsinfo ";
    let json;
    try {
        const response = await fetch(queryString);

        json = await response.json();
        const pageKey = Object.keys(json.query.pages)[0];
        const data = json.query.pages[pageKey].imageinfo[0].user;

        return data;
    } catch (error) {
        console.log(error);
        return error;
    }
};

export default getWikiCommonsUser;
