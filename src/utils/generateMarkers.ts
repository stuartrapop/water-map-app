import { Region } from "react-native-maps";
import { MarkerPointType, Point } from "@utils";

const generateMarkers = (
    jsonData: Point[],
    region: Region
): MarkerPointType[] => {
    const markersArray: MarkerPointType[] = [];

    for (let i = 0; i < jsonData.length; i++) {
        const markerPoint: MarkerPointType = {
            id: i,
            latitude: jsonData[i].geometry.coordinates[1],
            longitude: jsonData[i].geometry.coordinates[0],
            name: jsonData[i].properties.name,
            id_osm: jsonData[i].properties.id_osm,
            mapillary: jsonData[i].properties.mapillary || null,
            image: jsonData[i].properties.image || null,
            ewp: jsonData[i].properties.ewp || null,
            im: jsonData[i].properties.im || null,
        };
        if (
            markerPoint.latitude >
                region.latitude - 0.5 * region.latitudeDelta &&
            markerPoint.latitude <
                region.latitude + 0.5 * region.latitudeDelta &&
            markerPoint.longitude >
                region.longitude - 0.5 * region.longitudeDelta &&
            markerPoint.longitude <
                region.longitude + 0.5 * region.longitudeDelta
        ) {
            markersArray.push(markerPoint);
        }
    }

    return markersArray;
};

export default generateMarkers;
