import { DrawerNavigationProp } from "@react-navigation/drawer";
import { DrawNavigatorParams } from "@config";
import { Region } from "react-native-maps";

export type Point = {
    geometry: {
        coordinates: [number, number];
    };
    properties: {
        id_osm: number;
        im: string;
        image: string;
        name: string | null;
        mapillary: string | null;
        ewp: string | null;
    };
};

export interface Markers {
    id: number;
    id_osm: number;
    name: string | null;
    latitude: number;
    longitude: number;
    image: string | null;
    mapillary: string | null;
    ewp: string | null;
}
export type PopupProps = {
    name: string | null;
    id_osm: number;
    image: string | null;
    mapillary: string | null;
    ewp: string | null;
    latitude: number;
    longitude: number;
    closePopup: () => void;
    hasServerImage: boolean;
    navigation: DrawerNavigationProp<DrawNavigatorParams, "Home">;
};

export type mapStateType = {
    jsonData: [];
    bbox: string;
    zoom: number;

    showConnectionError: boolean;
    popupProps: PopupProps;
    region: Region;
};

export type MarkerPointType = {
    id: number;
    latitude: number;
    longitude: number;
    name: string | null;
    id_osm: number;
    mapillary: string | null;
    image: string | null;
    ewp: string | null;
    im: string | null;
};
