const colors = {
    lightGreen: "#dafaf7",
    darkPurple: "#120318",
    purple: "#221a36",
    lightPurple: "#bb29eb",
    win: "#86ff7d",
    loss: "#ff937d",
    draw: "#82cfff",
    waterMapBlue: "rgba(0, 61, 165, 1)",
    light: "#fff",
};

export default colors;
