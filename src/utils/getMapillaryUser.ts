const getMapillaryUser = async (imageKey: string): Promise<string | void> => {
    const access_token =
        "MLY|6146446665428512|6e662412d63604034ef6651cf156a1fb"; //mapillary client ID

    // const url = `https://a.mapillary.com/v3/images/${imageKey}?client_id=${client_id}`;

    const url = `https://graph.mapillary.com/:${imageKey}?access_token=${access_token}&fields=id`;

    try {
        const response = await fetch(url);

        const json = await response.json();
        console.log("data in mapillary function", json);

        const data = json.properties.username;

        return data;
    } catch (error) {
        console.log("error in getMapillaryUser", error);
        // return error;
    }
};

export default getMapillaryUser;
