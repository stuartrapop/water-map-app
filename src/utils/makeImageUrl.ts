import md5 from "md5";

function sanitizeFilename(filename: string) {
    // this doesn't cover all situations, but the following doesn't work either
    // return encodeURI(title.replace(/ /g, '_'));
    return (
        filename
            .replace(/\s+/g, "_")
            .replace(/,/g, "%2C")
            // .replace(/ü/g, '%C3%BC')
            .replace(/&/g, "%26")
    );
}
function unsanitizeFilename(mediaName: string) {
    return mediaName
        .replace(/_/g, " ")
        .replace(/%2C/g, ",")
        .replace(/%26/g, "&");
}
// Function based on https://github.com/simon04/wikimedia-commons-file-path/blob/master/index.js because npm import was not working
// based on https://github.com/derhuerst/commons-photo-url/blob/master/index.js
/**
 *
 * @param mediaName Filename, without "File:" or similar
 * @param width Width of returned image thumbnail, optional
 */
function getUrlFromMediaName(mediaName: string, width: number): string {
    // file = file.replace(/\s+/g, '_');
    const safe = sanitizeFilename(decodeURIComponent(mediaName));
    const base = "https://upload.wikimedia.org/wikipedia/commons";
    const hash: string = md5(
        decodeURIComponent(mediaName).replace(/\s+/g, "_")
    );
    const ns = `${hash[0]}/${hash[0]}${hash[1]}`;
    if (width) {
        // thumbnail
        const suffix = /tiff?$/i.exec(mediaName)
            ? ".jpg"
            : /svg$/i.exec(mediaName)
            ? ".png"
            : "";
        return `${base}/thumb/${ns}/${safe}/${width}px-${safe}${suffix}`;
    }

    // original
    return `${base}/${ns}/${safe}`;
}

/**
 * Function to obtain Wikimedia Commons media file name from URL
 * @param url URL of media file
 */
function getMediaNameFromUrl(url: string) {
    if (url === undefined) {
        return undefined;
    }
    const safe = url.split("/").pop();

    const mediaName = unsanitizeFilename(safe as string);
    return mediaName;
}
exports.getMediaNameFromUrl = getMediaNameFromUrl;

const makeImageUrl = (
    image: string | null,
    mapillary: string | null,
    ewp: string | null
): string => {
    if (image) {
        let imageDecode = decodeURIComponent(image);
        if (imageDecode.search("File:") != -1) {
            imageDecode = imageDecode.split("File:")[1];
        }

        const imageLinkString: string = getUrlFromMediaName(imageDecode, 250); //
        return imageLinkString;
    } else if (ewp) {
        return ewp;
    } else {
        const imageLinkString = `https://www.mapillary.com/embed?map_style=Mapillary%20streets&image_key=2227065947429675&style=photo`;
        return imageLinkString;
    }
};

export default makeImageUrl;
