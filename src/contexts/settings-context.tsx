import AsyncStorage from "@react-native-async-storage/async-storage";

import * as Localization from 'expo-localization';

import i18 from "../config/i18.config";
import React, {
    createContext,
    ReactElement,
    ReactNode,
    useContext,
    useEffect,
    useState,
} from "react";
import { Alert } from "react-native";

const languages = {
    EN: "en",
    DE: "de",
    ES: "es",
    FR: "fr",
    IT: "it",
};

type SettingsType = {
    language: keyof typeof languages;
    haptics: boolean;
    sounds: boolean;
};

const defaultSettings: SettingsType = {
    language: "EN",
    haptics: true,
    sounds: true,
};

type SettingsContextType = {
    settings: SettingsType | null;
    loadSettings: () => void;
    saveSetting: <T extends keyof SettingsType>(
        setting: T,
        value: SettingsType[T]
    ) => void;
};

export const SettingContext = createContext<SettingsContextType | undefined>(
    undefined
);

function useSettings(): SettingsContextType {
    const context = useContext(SettingContext);
    if (!context) {
        throw new Error("useSettings must be used witin a Settings Provider");
    }
    return context;
}

function SettingsProvider(props: { children: ReactNode; }): ReactElement {

    const [settings, setSettings] = useState<SettingsType | null>(null);

    const loadSettings = async () => {
        const allowedLanguages = ["EN", "FR", "DE", "IT", "ES"];
        let defaultLanguage: "EN" | "FR" | "DE" | "IT" | "ES" = "EN";
        const phoneLanguage = Localization.locale.toUpperCase().substring(0, 2) as typeof defaultLanguage;

        if (allowedLanguages.indexOf(phoneLanguage) > -1) {
            defaultLanguage = phoneLanguage;
        }

        try {
            const retrievedSettings = await AsyncStorage.getItem("@settings");
            console.log("retrieved data", retrievedSettings);
            retrievedSettings !== null
                ? setSettings(JSON.parse(retrievedSettings))
                : setSettings({
                    ...defaultSettings,
                    language: defaultLanguage,
                });
            i18.changeLanguage(settings?.language || "EN");
        } catch (error) {
            setSettings(defaultSettings);
        }
    };

    const saveSetting = async <T extends keyof SettingsType>(
        setting: T,
        value: SettingsType[T]
    ) => {
        try {
            const oldSettings = settings ? settings : defaultSettings;
            const newSettings = { ...oldSettings, [setting]: value };
            const jsonSettings = JSON.stringify(newSettings);
            await AsyncStorage.setItem("@settings", jsonSettings);
            setSettings(newSettings);
            i18.changeLanguage(newSettings?.language || "EN");
        } catch (error) {
            Alert.alert("Error has occured");
        }
    };

    useEffect(() => {
        loadSettings();
    }, []);

    return (
        <SettingContext.Provider
            {...props}
            value={{
                settings: settings,
                loadSettings: loadSettings,
                saveSetting: saveSetting,
            }}
        ></SettingContext.Provider>
    );
}

export { useSettings, SettingsProvider, languages };
