import React, { ReactElement, useEffect, useState } from "react";
import {
    View,
    Image,
    TouchableOpacity,
    StyleProp,
    ViewStyle,
    Dimensions,
} from "react-native";
import styles from "./header.styles";
import DropDownPicker from "react-native-dropdown-picker";
import { colors } from "@utils";
import { useSettings } from "@contexts/settings-context";

type HeaderProps = {
    style?: StyleProp<ViewStyle>;
    navigateHome: () => void;
    toggleDrawer: () => void;
    disactivateHome?: boolean;
};

type valueType = "EN" | "DE" | "ES" | "FR" | "IT";

export default function Header({
    toggleDrawer,
    navigateHome,
    disactivateHome = false,
    style,
}: HeaderProps): ReactElement | null {
    const { settings, saveSetting } = useSettings();

    const [open, setOpen] = useState(false);
    const [value, setValue] = useState<valueType>("EN");

    useEffect(() => {
        setValue(settings?.language || "EN");
    }, [settings]);
    const [items, setItems] = useState([
        { label: "EN", value: "EN" },
        { label: "DE", value: "DE" },
        { label: "ES", value: "ES" },
        { label: "FR", value: "FR" },
        { label: "IT", value: "IT" },
    ]);

    DropDownPicker.setListMode("MODAL");
    if (!settings) return null;
    return (
        <View style={[styles.container, style]}>
            <TouchableOpacity onPress={toggleDrawer}>
                <Image
                    style={styles.hamburger}
                    source={require("@assets/dropdownblue.png")}
                />
            </TouchableOpacity>
            <TouchableOpacity
                style={styles.logoContainer}
                disabled={disactivateHome}
                onPress={navigateHome}
            >
                <Image
                    style={styles.logo}
                    source={require("@assets/logoWaterMap.png")}
                />
            </TouchableOpacity>
            <View
                style={{
                    width: 75,
                    zIndex: 500,
                    marginRight: 30,
                }}
            >
                <DropDownPicker
                    onChangeValue={(value) => {
                        saveSetting("language", value as valueType);
                    }}
                    itemSeparator={true}
                    modalContentContainerStyle={{
                        backgroundColor: "#fff",
                        width: "50%",
                        height: "50%",
                        alignSelf: "center",
                        marginTop:
                            Dimensions.get("screen").width > 1000 ? 50 : 30,
                    }}
                    modalProps={{}}
                    labelStyle={{
                        color: colors.waterMapBlue,
                        fontSize:
                            Dimensions.get("screen").width > 1000 ? 24 : 15,
                        fontWeight: "bold",
                        margin: 0,
                    }}
                    arrowIconStyle={{
                        marginLeft: -20,
                    }}
                    style={{
                        borderWidth: 0,
                        padding: 0,
                    }}
                    selectedItemLabelStyle={{}}
                    listItemLabelStyle={{
                        color: colors.waterMapBlue,
                        fontSize:
                            Dimensions.get("screen").width > 1000 ? 24 : 15,
                        marginRight: 10,
                        zIndex: 99,
                    }}
                    setOpen={setOpen}
                    open={open}
                    value={value}
                    items={items}
                    setValue={setValue}
                    setItems={setItems}
                />
            </View>
        </View>
    );
}
