import { Dimensions, Platform, StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Platform.OS === "android" ? 25 : 0,
        width: Dimensions.get("screen").width,
        backgroundColor: "#fff",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        zIndex: 100,
    },
    hamburger: {
        width: Dimensions.get("screen").width * 0.05,
        height: Dimensions.get("screen").width * 0.05,
        minHeight: 30,
        minWidth: 30,
        marginLeft: 20,
    },
    logo: {
        height: "50%",
        minHeight: 50,
        resizeMode: "contain",
        maxWidth: "70%",
        marginHorizontal: "20%",
    },
    logoContainer: {
        width: "60%",
    },
    dropdown: {
        marginRight: 30,
    },
});

export default styles;
