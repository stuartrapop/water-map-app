import { colors } from "@utils";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.light,
    },
    footnote: {
        marginTop: 5,
        textAlign: "left",
        color: colors.darkPurple,
        fontSize: 14,
    },
    header: {
        flex: 1,
        backgroundColor: colors.light,
    },
    main: {
        flex: 10,
    },
    pageContainer: {
        margin: 20,
        width: "90%",
    },
    title: {
        marginTop: 30,
        marginBottom: 39,
        fontSize: 18,
        fontWeight: "bold",
    },
    span: {
        color: colors.darkPurple,
        backgroundColor: colors.light,
        fontSize: 15,
        textAlign: "left",
    },
    h2: {
        fontWeight: "bold",
        fontSize: 15,
        textAlign: "left",
    },
    p: {
        fontSize: 15,
    },

    a: {
        margin: 0,

        padding: 0,
        textDecorationLine: "underline",
    },
    socialMediaContainer: {
        width: "100%",
        flexDirection: "row",
        justifyContent: "flex-start",
        marginTop: 5,
    },
    socialMediaIcon: {
        width: 30,
        height: 30,
        marginRight: 10,
        marginLeft: 10,
    },
});

export default styles;
