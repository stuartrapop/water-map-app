import React from "react";
import { ReactElement } from "react";
import {
    View,
    ScrollView,
    SafeAreaView,
    Text,
    Linking,
    Image,
    TouchableOpacity,
} from "react-native";
import HTMLView from "react-native-htmlview";

import Header from "../header/header";
import styles from "./page-container.styles";

type PageContainerProps = {
    toggleDrawer: () => void;
    navigateHome: () => void;
    title: string;
    page: string;
};

const stripMultipleSpaces = (inputText: string): string => {
    return (
        "" +
        inputText
            .replace(/ +(?= )/g, "")
            .replace(/\n\s*\n/g, "\n")
            .replace(/ +</g, "<")
            .replace(/>+ /g, ">")
            .replace(/ +-/g, "-")
    );
};

export default function PageContainer({
    toggleDrawer,
    navigateHome,
    title,
    page,
}: PageContainerProps): ReactElement {
    return (
        <SafeAreaView style={styles.container}>
            <Header
                style={styles.header}
                toggleDrawer={toggleDrawer}
                navigateHome={navigateHome}
            />
            <View style={styles.main}>
                <ScrollView contentContainerStyle={{}}>
                    <View style={styles.pageContainer}>
                        <Text style={styles.title}>{title}</Text>
                        <HTMLView
                            stylesheet={styles}
                            value={`<span>${stripMultipleSpaces(page)}</span>`}
                        />

                        <Text
                            onPress={() => {
                                Linking.openURL(`https://water-map.org`);
                            }}
                            style={styles.footnote}
                        >
                            {" "}
                            ©2021 Water-Map.org
                        </Text>
                        <View style={styles.socialMediaContainer}>
                            <TouchableOpacity
                                onPress={() => {
                                    Linking.openURL(
                                        `https://twitter.com/water_map_?lang=en`
                                    );
                                }}
                            >
                                <Image
                                    style={styles.socialMediaIcon}
                                    source={require("@assets/twitter.png")}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    Linking.openURL(
                                        `https://www.facebook.com/WaterMapFB/`
                                    );
                                }}
                            >
                                <Image
                                    style={styles.socialMediaIcon}
                                    source={require("@assets/facebook.png")}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    Linking.openURL(
                                        `https://www.linkedin.com/company/water-map/?viewAsMember=true`
                                    );
                                }}
                            >
                                <Image
                                    style={styles.socialMediaIcon}
                                    source={require("@assets/linkedin.png")}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    Linking.openURL(
                                        `https://www.instagram.com/water_map_/`
                                    );
                                }}
                            >
                                <Image
                                    style={styles.socialMediaIcon}
                                    source={require("@assets/instagram.png")}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        </SafeAreaView>
    );
}
