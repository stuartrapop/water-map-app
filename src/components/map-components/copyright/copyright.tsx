import React, { ReactElement } from "react";
import { View, Text, Linking } from "react-native";
import { colors } from "@utils";

const Copyright = (): ReactElement => {
    return (
        <View
            style={{
                paddingHorizontal: 3,
                backgroundColor: "rgba(255, 255, 255, 0.5)",
                paddingVertical: 3,
                zIndex: 2,
                position: "absolute",
                bottom: 10,
                right: 20,
                maxWidth: 200,
                borderRadius: 5,
            }}
        >
            <Text
                onPress={() => {
                    Linking.openURL(`https://openstreetmap.org`);
                }}
                style={{
                    flex: 1,
                    textAlign: "center",
                    fontSize: 12,
                    fontWeight: "bold",

                    color: colors.waterMapBlue,
                    flexWrap: "wrap",
                }}
            >
                Data from ©OpenStreetMap
            </Text>
        </View>
    );
};

export default Copyright;
