import { mapStateType } from "@utils";
import React, { ReactElement } from "react";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import appJon from "../../../../app.json";


// navigator.geolocation = require('@react-native-community/geolocation');
// navigator.geolocation = require('react-native-geolocation-service');
type Region = {
    latitude: number;
    longitude: number;
    latitudeDelta: number;
    longitudeDelta: number;
};

type Props = {
    region: Region;
    mapState: mapStateType;
    setMapState: (newState: mapStateType) => void;
    mapRef: any;
};
const GooglePlacesInput = ({
    region,
    mapState,
    setMapState,
    mapRef,
}: Props): ReactElement => {
    return (
        <GooglePlacesAutocomplete
            placeholder="Search"
            fetchDetails={true}
            GooglePlacesSearchQuery={{
                rankby: "distance",
            }}
            onPress={(data, details = null) => {
                // 'details' is provided when fetchDetails = true
                const newRegion = {
                    ...region,
                    latitude: details?.geometry.location.lat ?? region.latitude,
                    longitude:
                        details?.geometry.location.lng ?? region.longitude,
                };
                setMapState({ ...mapState, region: newRegion });
                mapRef.current.animateToRegion(newRegion, 1200);
            }}
            query={{

                key: appJon.expo.ios.config.googleMapsApiKey,
                // key: "AIzaSyB2_UUL34nxyTXzphMHfqPPm7kHnYFP8qg",
                language: "en",
                location: `${region.latitude}, ${region.longitude}`,
            }}
            styles={{
                container: {
                    flex: 1,
                    position: "absolute",
                    width: "100%",
                    zIndex: 1,
                    top: 7,
                },
                textInputContainer: {
                    backgroundColor: "transparent",
                    height: 48,

                    marginHorizontal: 7,
                    borderTopWidth: 0,
                    borderBottomWidth: 0,
                    borderRadius: 5,
                },
                textInput: {
                    height: 45,
                    margin: 0,
                    borderRadius: 10,
                    paddingTop: 0,
                    paddingBottom: 0,
                    paddingLeft: 8,
                    paddingRight: 0,
                    padding: 0,
                    marginTop: 0,
                    marginLeft: 0,
                    marginRight: 0,
                    fontSize: 16,
                    borderColor: "grey",
                    borderWidth: 0.3,
                },

                listView: {
                    backgroundColor: "white",
                    fontSize: 16,
                    fontWeight: "bold",
                    marginHorizontal: 7,
                    borderRadius: 3,
                },
            }}
        />
    );
};

export default GooglePlacesInput;
