import React, { ReactElement } from "react";
import { TouchableOpacity, TouchableOpacityProps } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";

type LocationButtonProps = {
    gotoCurrentPosition: () => void;
} & TouchableOpacityProps;

export default function LocationButton({
    gotoCurrentPosition,
}: LocationButtonProps): ReactElement {
    return (
        <TouchableOpacity
            style={{
                position: "absolute",
                flex: 1,
                alignItems: "center",
                justifyContent: "center",
                bottom: 100,
                right: 15,
                width: 50,
                height: 50,
                borderRadius: 25,
                backgroundColor: "rgba(255,255,255,0.7)",
            }}
            onPress={gotoCurrentPosition}
        >
            <MaterialIcons name="my-location" size={30} color="black" />
        </TouchableOpacity>
    );
}
