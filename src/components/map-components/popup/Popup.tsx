import React, { ReactElement, useEffect, useState } from "react";
import { AntDesign, MaterialCommunityIcons } from "@expo/vector-icons";
import * as Location from "expo-location";
import { useTranslation } from "react-i18next";
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Platform,
    Dimensions,
} from "react-native";

import ProgressiveImage from "./progressive-image/progressive-image";
import ServerImageOrDefault from "./server-image-or-default/server-image-or-default";

import * as Linking from "expo-linking";
import {
    getWikiCommonsUser,
    makeImageUrl,
    PopupProps,
} from "@utils";

const isLargeScreen = Dimensions.get("screen").width > 800;

export default function Popup({
    name,
    image,
    mapillary,
    id_osm,
    ewp,
    latitude,
    longitude,
    closePopup,
    navigation,
    hasServerImage,
}: PopupProps): ReactElement {
    const [wikiPhrase, setWikiPhrase] = useState<ReactElement | null>(null);
    const [ewpPhrase, setEwpPhrase] = useState<ReactElement | null>(null);
    const [expandPhoto, setExpandPhoto] = useState(false);
    const [addNewPhotoLink, setAddNewPhotoLink] = useState(false);
    // const [mapillaryPhrase, setMapillaryPhrase] = useState<ReactElement | null>(
    //     null
    // );
    const [reverseCodePhrase, setReverseCodePhrase] =
        useState<ReactElement | null>(null);

    const { t } = useTranslation();

    useEffect(() => {
        let mounted = true;
        Location.reverseGeocodeAsync({
            latitude,
            longitude,
        }).then((response) => {
            if (mounted && response[0]) {
                if ("street" in response[0])
                    setReverseCodePhrase(
                        <Text style={styles.title}>
                            {response[0].street}, {response[0].city}{" "}
                        </Text>
                    );
            }
        });

        if (image) {
            getWikiCommonsUser(image).then((user) => {
                let imageName: string;
                if (image.indexOf("File:") == -1) {
                    imageName = image;
                } else {
                    imageName = image.substring(
                        image.indexOf("File:") + 5,
                        image.length
                    );
                }
                if (mounted && user) {
                    setWikiPhrase(
                        <Text
                            style={styles.source}
                            onPress={() => {
                                Linking.openURL(
                                    `https://commons.wikimedia.org/wiki/File:${imageName}`
                                );
                            }}
                        >
                            Src:{" "}
                            <Text style={{ textDecorationLine: "underline" }}>
                                Wikimedia
                            </Text>
                            {"  "}
                            {user}
                        </Text>
                    );
                }
            });
        } else if (ewp) {
            try {
                if (mounted) {
                    setEwpPhrase(
                        <Text
                            style={styles.source}
                            onPress={() => {
                                Linking.openURL(`https://water-map.org`);
                            }}
                        >
                            Src:
                            <Text style={{ textDecorationLine: "underline" }}>
                                Water-Map.org
                            </Text>
                        </Text>
                    );
                }
            } catch (error) {
                console.log(error);
            }
        }
        // else if (mapillary) {

        //     console.log('mapillary,', mapillary);
        //     try {
        //         getMapillaryUser(mapillary).then((user) => {
        //             if (mounted && user) {
        //                 setMapillaryPhrase(
        //                     <Text
        //                         style={styles.source}
        //                         onPress={() => {
        //                             Linking.openURL(
        //                                 `https://www.mapillary.com/map/im/${mapillary}`
        //                             );
        //                         }}
        //                     >
        //                         Src:
        //                         <Text style={{ textDecorationLine: "underline" }}>
        //                             Mapillary{" "}
        //                         </Text>
        //                         {user}
        //                     </Text>
        //                 );
        //             }
        //         });
        //     } catch (error) {
        //         console.error("error in mapillary user", error);
        //     }

        // }

        return function cleanup() {
            mounted = false;
        };
    }, []);
    const url =
        image || ewp ? makeImageUrl(image, mapillary, ewp) : null;

    const photo = url ? (
        <ProgressiveImage
            style={{ width: "100%", height: "100%" }}
            source={{
                uri: url,
            }}
        />
    ) : (
        <ServerImageOrDefault
            style={{ width: "100%", height: "100%" }}
            id_osm={id_osm}
            hasServerImage={hasServerImage}
            setAddNewPhotoLink={setAddNewPhotoLink}
            setEwpPhrase={setEwpPhrase}
        />
    );

    return (
        <View style={styles.container}>
            <View style={styles.iconBox}>
                <TouchableOpacity
                    style={styles.closeButton}
                    onPress={closePopup}
                >
                    <AntDesign color="#000000" name="close" size={24} />
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.directionsButton}
                    onPress={() => {
                        Platform.OS === "ios"
                            ? Linking.openURL(
                                `http://maps.apple.com/maps?q=${latitude},${longitude}`
                            )
                            : Linking.openURL(
                                `geo:${latitude},${longitude}?q=${latitude},${longitude}`
                            );
                    }}
                >
                    <MaterialCommunityIcons
                        name="directions"
                        size={33}
                        color="black"
                    />
                </TouchableOpacity>
            </View>

            {name ? (
                <Text style={styles.title}>{name}</Text>
            ) : (
                reverseCodePhrase
            )}
            <Text style={styles.title}>
                {t("camera.latitude")}: {Math.round(1000 * latitude) / 1000}
            </Text>
            <Text style={styles.title}>
                {t("camera.longitude")}: {Math.round(1000 * longitude) / 1000}
            </Text>
            <TouchableOpacity
                style={[
                    styles.image,
                    expandPhoto && {
                        width: isLargeScreen ? 400 : 280,
                        height: isLargeScreen ? 400 : 280,
                    },
                ]}
                onPress={() => {
                    setExpandPhoto(!expandPhoto);
                }}
            >
                {photo}
            </TouchableOpacity>
            {(image || ewpPhrase) && (
                <View
                    style={{ height: isLargeScreen ? 90 : 45, width: "100%" }}
                >
                    {wikiPhrase}
                    {ewpPhrase}

                </View>
            )}

            {addNewPhotoLink && (
                <View
                    style={{ height: isLargeScreen ? 90 : 45, width: "100%" }}
                >
                    <Text
                        style={[
                            styles.source,
                            { textDecorationLine: "underline" },
                        ]}
                        onPress={() => {
                            closePopup();
                            navigation.navigate("Camera", {
                                id_osm: id_osm,
                            });
                        }}
                    >
                        {t("camera.fountainPopupAdd")}
                    </Text>
                </View>
            )}
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        alignItems: "flex-start",
        backgroundColor: "#fff",
        borderColor: "grey",
        borderRadius: 10,
        borderWidth: 1,
        bottom: 50,
        flex: 1,
        justifyContent: "flex-start",
        left: 20,
        overflow: "hidden",
        padding: 12,
        position: "absolute",
        zIndex: 99,
    },
    closeButton: {
        height: isLargeScreen ? 50 : 30,
        width: isLargeScreen ? 50 : 30,
        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        borderColor: "#000",
        borderWidth: 1,
        backgroundColor: "#fff",
    },
    image: {
        width: isLargeScreen ? 320 : 160,
        height: isLargeScreen ? 320 : 160,
        marginVertical: 5,
    },
    iconBox: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        width: isLargeScreen ? 300 : 150,
        marginBottom: 5,
    },
    directionsButton: {
        height: 35,
        width: 35,
        borderRadius: 3,
        justifyContent: "center",
        alignItems: "center",
        borderColor: "#000",
        borderWidth: 0.5,
        backgroundColor: "#fff",
    },
    title: {
        fontWeight: "600",
        width: isLargeScreen ? 320 : 160,
        flex: 1,
        flexWrap: "wrap",
        fontSize: isLargeScreen ? 20 : 14,
    },
    source: {
        width: isLargeScreen ? 320 : 160,
        flex: 1,
        flexWrap: "wrap",
        fontSize: isLargeScreen ? 20 : 14,
    },
});
