import React, { ReactElement, useRef } from "react";
import { View, ImageProps, Animated } from "react-native";

import styles from "./progressive-image.styles";

const ProgressiveImage = ({
    style,
    source,
    ...props
}: ImageProps): ReactElement => {
    const imageAnimated = useRef(new Animated.Value(0)).current;

    const onImageLoad = () => {
        Animated.timing(imageAnimated, {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true,
        }).start();
    };
    return (
        <View style={styles.container}>
            <Animated.Image
                style={[styles.imageOverlay, { opacity: imageAnimated }, style]}
                {...props}
                source={source}
                onLoad={onImageLoad}
            />
        </View>
    );
};
export default ProgressiveImage;
