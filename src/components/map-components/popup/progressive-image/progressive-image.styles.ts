import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    imageOverlay: {
        position: "absolute",
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
        backgroundColor: "white",
    },
    container: {
        backgroundColor: "#e1e4e8",
        width: "100%",
        height: "100%",
    },
});

export default styles;
