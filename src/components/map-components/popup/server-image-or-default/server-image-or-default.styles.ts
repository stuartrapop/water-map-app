import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    imageOverlay: {
        position: "absolute",
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
        backgroundColor: "white",
    },
    container: {
        width: "100%",
        height: "100%",
    },
    source: {
        width: 160,
        flex: 1,
        flexWrap: "wrap",
        fontSize: 14,
    },
});

export default styles;
