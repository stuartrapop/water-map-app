import React, { ReactElement, useRef, useState } from "react";
import {
    View,
    Animated,
    ImageBackground,
    ImageSourcePropType,
    ImageStyle,
    Text,
    Linking,
} from "react-native";
import { ServerUrl } from "@utils";

import styles from "./server-image-or-default.styles";

type ServerImageOrDefaultProps = {
    style: ImageStyle;
    id_osm: number;
    hasServerImage: boolean;
    setAddNewPhotoLink: (value: boolean) => void;
    setEwpPhrase: (value: ReactElement | null) => void;
};

const ServerImageOrDefault = ({
    style,
    hasServerImage,
    id_osm,
    setAddNewPhotoLink,
    setEwpPhrase,
    ...props
}: ServerImageOrDefaultProps): ReactElement => {
    const [photoSource, setPhotoSource] = useState<ImageSourcePropType>(
        hasServerImage
            ? {
                uri: `${ServerUrl}/photos/EWP${id_osm}.jpg?${Date.now()}`,
                cache: "reload",
                headers: {
                    Pragma: "no-cache",
                },
            }
            : require("@assets/defaultPhoto.png")
    );

    const imageAnimated = useRef(new Animated.Value(0)).current;

    const onImageLoad = () => {
        Animated.timing(imageAnimated, {
            toValue: 1,
            duration: 700,
            useNativeDriver: true,
        }).start();

    };
    return (
        <View style={styles.container}>
            <ImageBackground
                source={require("@assets/defaultPhoto.png")}
                style={{ width: "100%", height: "100%", opacity: 0.1 }}
                resizeMode="cover"
            ></ImageBackground>
            <Animated.Image
                style={[styles.imageOverlay, { opacity: imageAnimated }, style]}
                {...props}
                source={photoSource}
                onLoad={() => {
                    onImageLoad();
                    setEwpPhrase(
                        <Text
                            style={styles.source}
                            onPress={() => {
                                Linking.openURL(`https://water-map.org`);
                            }}
                        >
                            Src:
                            <Text style={{ textDecorationLine: "underline" }}>
                                Water-Map.org
                            </Text>
                        </Text>
                    );
                    if (!hasServerImage) {
                        setAddNewPhotoLink(true);
                        setEwpPhrase(null);
                    } else {
                        setEwpPhrase(
                            <Text
                                style={styles.source}
                                onPress={() => {
                                    Linking.openURL(`https://water-map.org`);
                                }}
                            >
                                Src:
                                <Text style={{ textDecorationLine: "underline" }}>
                                    Water-Map.org
                                </Text>
                            </Text>
                        );
                    }
                }}
                onError={() => {
                    // eslint-disable-next-line @typescript-eslint/no-var-requires
                    setPhotoSource(require("@assets/defaultPhoto.png"));
                    setAddNewPhotoLink(true);

                }}
            />
        </View>
    );
};
export default ServerImageOrDefault;
