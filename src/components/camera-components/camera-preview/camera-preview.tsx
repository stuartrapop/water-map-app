import React, { ReactElement } from "react";
import { DrawNavigatorParams } from "@config";
import { DrawerNavigationProp } from "@react-navigation/drawer";
import { CameraCapturedPicture } from "expo-camera";
import {
    ImageBackground,
    TouchableOpacity,
    View,
    Text,
    StyleSheet,
    Dimensions,
} from "react-native";
import { useTranslation } from "react-i18next";

type CameraPreviewProps = {
    photo: CameraCapturedPicture | undefined;
    retakePicture: () => void;
    savePhoto: (photo: CameraCapturedPicture) => void;
    navigation: DrawerNavigationProp<DrawNavigatorParams, "Camera">;
};
const CameraPreview = ({
    photo,
    retakePicture,
    savePhoto,
    navigation,
}: CameraPreviewProps): ReactElement => {
    const { t } = useTranslation();
    return (
        <View
            style={{
                backgroundColor: "transparent",
                flex: 1,
                width: "100%",
                height: "100%",
            }}
        >
            <View
                style={{
                    backgroundColor: "transparent",
                    flex: 9,
                    width: "100%",
                    height: "100%",
                }}
            >
                <ImageBackground
                    source={{ uri: photo && photo.uri }}
                    style={{
                        flex: 1,
                    }}
                ></ImageBackground>
            </View>
            <View
                style={{
                    flex: 1,
                    paddingVertical: 20,
                }}
            >
                <View
                    style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                    }}
                >
                    <TouchableOpacity
                        onPress={retakePicture}
                        style={styles.buttons}
                    >
                        <Text style={styles.buttonsText}>
                            {t("camera.labelRetake")}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            if (photo) savePhoto(photo);
                        }}
                        style={styles.buttons}
                    >
                        <Text style={styles.buttonsText}>
                            {t("camera.labelSave")}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            navigation.navigate("Home");
                        }}
                        style={styles.buttons}
                    >
                        <Text style={styles.buttonsText}>
                            {t("camera.labelCancel")}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    buttons: {
        width: Dimensions.get("screen").width > 1000 ? 160 : 120,
        height: 40,
        alignItems: "center",
    },
    buttonsText: {
        color: "black",
        fontSize: Dimensions.get("screen").width > 1000 ? 22 : 15,
    },
});

export default CameraPreview;
