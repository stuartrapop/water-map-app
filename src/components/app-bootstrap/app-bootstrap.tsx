import React, { ReactElement, ReactNode, useEffect, useState } from "react";

import {
    DeliusUnicase_400Regular,
    DeliusUnicase_700Bold
} from "@expo-google-fonts/delius-unicase";
import * as Font from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';

type AppBootstrapProps = {
    children: ReactNode;
};

SplashScreen.preventAutoHideAsync();

export default function AppBootstrap({
    children,
}: AppBootstrapProps): ReactElement | null {


    const [appIsReady, setAppIsReady] = useState(false);
    const [showSplash, setShowSplash] = useState(true);


    useEffect(() => {
        async function prepare() {
            try {
                await Font.loadAsync({
                    DeliusUnicase_400Regular,
                    DeliusUnicase_700Bold,
                });
            } catch (e) {
                console.warn(e);
            } finally {
                setAppIsReady(true);
                console.log("waiting for load", appIsReady);
            }
        }

        prepare();
    }, []);

    const removeSplashScreen = async () => {

        // This tells the splash screen to hide immediately! If we call this after
        // `setAppIsReady`, then we may see a blank screen while the app is
        // loading its initial state and rendering its first pixels. So instead,
        // we hide the splash screen once we know the root view has already
        // performed layout.
        await SplashScreen.hideAsync();
        setShowSplash(false);
        console.log("app is ready", appIsReady);
    };


    if (appIsReady) {
        removeSplashScreen();
    };
    if (!appIsReady || showSplash) {
        return null;
    }



    console.log("app is ready before render", appIsReady);
    return (
        <>

            {children}
        </ >);

    // return (
    //     <View
    //         style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
    //         onLayout={onLayoutRootView}>
    //         {children}

    //     </View>
    // );

    // return fontLoaded ? (
    //     <>{children}</>
    // ) : Platform.OS === "ios" ? (
    //     <View style={{ flex: 1 }}>
    //         <Image
    //             style={{ width: "100%", height: "100%" }}
    //             source={require("@assets/splash.png")}
    //         />
    //     </View>
    // ) : (
    //     <AppLoading />
    // );
}
