import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import {
    Plastic,
    Fountain,
    Home,
    Project,
    Tap,
    Bottle,
    CameraScreen,
} from "@screens";
import { useTranslation } from "react-i18next";
import { colors } from "@utils";

export type DrawNavigatorParams = {
    Bottle: undefined;
    Fountain: undefined;
    Home: undefined;
    Plastic: undefined;
    Project: undefined;
    Tap: undefined;
    Camera: { id_osm: number };
    Facebook: undefined;
};
const Drawer = createDrawerNavigator<DrawNavigatorParams>();

function MyDrawer() {
    const { t } = useTranslation();

    return (
        <Drawer.Navigator
            initialRouteName="Home"
            screenOptions={{
                drawerStyle: {
                    backgroundColor: colors.light,
                    width: 300,
                },
                drawerLabelStyle: { fontSize: 15 },
            }}
        >
            <Drawer.Screen
                name="Home"
                component={Home}
                options={{
                    headerShown: false,
                }}
            />
            <Drawer.Screen
                name="Plastic"
                component={Plastic}
                options={{
                    headerShown: false,
                    drawerLabel: t("plastic.label"),
                }}
            />
            <Drawer.Screen
                name="Project"
                component={Project}
                options={{
                    headerShown: false,
                    drawerLabel: t("project.label"),
                }}
            />
            <Drawer.Screen
                name="Tap"
                component={Tap}
                options={{ headerShown: false, drawerLabel: t("tap.label") }}
            />
            <Drawer.Screen
                name="Bottle"
                component={Bottle}
                options={{ headerShown: false, drawerLabel: t("bottle.label") }}
            />
            <Drawer.Screen
                name="Fountain"
                component={Fountain}
                options={{
                    headerShown: false,
                    drawerLabel: t("fountain.label"),
                }}
            />
            <Drawer.Screen
                name="Camera"
                component={CameraScreen}
                options={{
                    headerShown: false,
                    drawerLabel: () => null,
                    title: undefined,
                    drawerIcon: () => null,
                }}
            />
        </Drawer.Navigator>
    );
}

export default function DrawerNavigator(): React.ReactElement {
    return (
        <NavigationContainer>
            <MyDrawer />
        </NavigationContainer>
    );
}
