export { default as Navigator } from "./drawer-navigator";

export { DrawNavigatorParams } from "./drawer-navigator";

export { default as i18n } from "./i18.config";
