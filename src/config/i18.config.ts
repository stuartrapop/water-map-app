import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import de from "./locales/de";
import en from "./locales/en";
import es from "./locales/es";
import fr from "./locales/fr";
import it from "./locales/it";

//empty for now
const resources = {
    EN: en,
    ES: es,
    DE: de,
    FR: fr,
    IT: it,
};

i18n.use(initReactI18next).init({
    resources,
    //language to use if translations in user language are not available
    fallbackLng: "EN",
    react: {
        bindI18n: "languageChanged",
    },
});

export default i18n;
