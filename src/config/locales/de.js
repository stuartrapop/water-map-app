/* eslint-disable no-irregular-whitespace */
export default {
    translation: {
        camera: {
            closer: 'Sie müssen sich im Umkreis von 20 Metern befinden, damit die Kamera funktioniert. ',
            fountainPopupAdd: 'Dieses Bild hinzufügen/aktualisieren',
            fountainPopupUpdate: 'Dieses Foto aktualisieren',
            latitude: 'Breitengrad',
            longitude: 'Längengrad',
            success: 'Ihr Foto wurde gespeichert.',
            thanks: 'Ich danke Ihnen für Ihren Beitrag.',
            saveError:
                'Verbindungsproblem. Ihr Foto konnte nicht gespeichert werden.',
            dataError:
                'Verbindungsproblem. Bitte versuchen Sie es noch einmal, wenn es wiederhergestellt ist.',
        },
        bottle: {
            label: 'Adoptiere eine Trinkflasche ',
            title: 'Adoptiere eine Trinkflasche ',
            page: `<h2>Das Befüllen deiner Trinkflasche mit Leitungswasser bedeutet: </h2>

            <p>- hydriert zu bleiben
            - Geld zu sparen, weil es gratis ist
            - Plastikmüll durch Vermeclass ung zu reduzieren</p>

            <h2>Wussten Sie schon? </h2>

            <p>Jede Herstellung von Plastik- oder Glasflaschen hat ökologische Auswirkungen ... </p>

            <h2> Plastikflaschen </h2>

            <p>Um eine Ein-Liter- Plastikflasche herzustellen, benötigt man 100 ml Öl, 80 g Kohle, 42 Liter Gas und ... 2 Liter Wasser. </p>

            <h2> Glasflaschen </h2>

            <p>Laut einer von Tétra Pack in Auftrag gegebenen Studie  verursacht die Herstellung einer Ein-Liter-Glasflaschen Emissionen in Höhe von 325 g CO2.</p>

            <p>Und das, ohne den CO2-Fußabdruck des LKW-Transports und des energieintensiven Recyclings zu berücksichtigen...</p>

            <h2> Fazit: Der beste Abfall ist der, der nicht produziert wird!</h2>

            `,
        },
        facebook: {
            label: 'Facebook',
            link: 'href="https://www.facebook.com/EuropeanWaterProject/"',
        },
        plastic: {
            label: 'Das Plastikproblem',
            title: 'Das Plastikproblem',
            page: `<h2>Einige Schlüsselzahlen</h2>

            <p>- Jede Minute werden weltweit eine Million Plastikflaschen gekauft, Tendenz steigend, und etwa 30 % davon werden recycelt.
            - 1/3 des Plastikmülls, den man in der Natur und an den Stränden findet, sind Plastikflaschen.
            - Sie verschmutzen das Land, die Flüsse und die Meere. Sie töten jedes Jahr 1,5 Millionen Tiere, die Mikroplastik aufgenommen haben.
            - Für die Herstellung einer 1-L-Plastikflasche werden 100 ml Öl und 2 Liter Wasser benötigt. Wenn die Situation so weitergeht, wird es zu einer starken Umweltverschmutzung kommen.</p>

            <p>Wenn das so weitergeht, wird es im Jahr 2050 mehr Plastik als Fisch in den Ozeanen geben! </p>

            <h2>Auswirkungen auf die Gesundheit</h2>

            <p>Viele Studien zeigen, dass der Mensch pro Woche etwa 5 Gramm Mikroplastik aufnimmt (es ist in Fischen, Tieren und im Wasser, das wir trinken, enthalten).</p>

            <p>Ein weiterer Aspekt, über den weniger nachgedacht wird, ist das Vorhandensein von Mikroplastik in abgefülltem Wasser. Es soll im Durchschnitt doppelt so hoch sein wie im Leitungswasser, wenn man den Beobachtungen von Orb Media mit der State University of New York Glauben schenken darf.</p>



            <h2>Von der Herstellung bis zum Abfall: Plastik in Flaschen ist umweltschädlich</h2>

            <p>- Das Plastik in Wasserflaschen besteht aus chemischen Verbindungen, die schädliche Folgen für unsere Gesundheit haben können.
            - Die Flaschen sind aus PET-Kunststoff hergestellt, der nach 2 bis 3 Mal nicht mehr recycelt werden kann (er verliert seine Festigkeitseigenschaften).
            - PET kann in kleinere Stücke zerlegt und zu anderen Flaschen oder Textilfasern (Decken, Flusen) verarbeitet werden, was jedoch nur in 20 % der Fälle geschieht.
            - Der größte Teil der Abfälle wird daher vergraben (auf Deponien, 35 %) oder verbrannt (Verbrennungsanlagen, 29 %). Es wird gesagt, dass sie "zurückgewonnen" wird (die freigesetzte Wärme ist eine Energiequelle), aber das ist nicht tugendhaft und führt wiederum zu Umweltverschmutzung.</p>



            <h2>Ist Wasser aus der Flasche besser als Leitungswasser?</h2>

            <p>Marken haben viele Menschen davon überzeugt, dass abgefülltes Wasser besser ist als Leitungswasser. Dennoch zeigen viele Blindtests, dass Leitungswasser dem Wasser aus der Flasche vorgezogen wird (das Wasser aus der Flasche ist so gefiltert, dass es nicht mehr gut schmeckt).</p>

            <p>In Frankreich ist das Leitungswasser besonders gesund und wird streng kontrolliert. Dennoch gehört Frankreich zu den 5 Ländern der Welt, die am meisten Plastikflaschen verbrauchen, hinter Mexiko, Thailand (wo es keinen flächendeckenden Zugang zu Trinkwasser gibt), Italien und Deutschland. Im Falle Frankreichs handelt es sich tatsächlich um Mineralwasser, wobei nur 18 % des Verbrauchs auf Wasser mit Kohlensäure entfallen.</p>



            <h2>Was können wir tun?</h2>

            <p>Endlich eine gute Nachricht: Sie können etwas tun, um das Ausmaß dieser Verschmutzung zu verringern! Wie? Durch das Trinken von Leitungswasser in allen Ländern, in denen es sicher zu trinken ist!</p>
             `,
        },

        project: {
            label: 'Über Water-Map',
            title: 'Über Water-Map',
            page: `<h2>Water-Map wurde 2019 von der Nichtregierungsorganisation European Water Project ins Leben gerufen und verfolgt die folgenden Ziele:</h2>

            <p>- Beteiligung und Beitrag zur Reduzierung von Plastikabfällen durch Förderung des Verbrauchs von Leitungswasser anstelle von Flaschenwasser.
            - Förderung der Nutzung von Water-Map: Sie ermöglicht es den Menschen, die nächstgelegenen Trinkwasserstellen aufzusuchen und ihre Wasserflaschen kostenlos aufzufüllen.
            - Unterstützung von Maßnahmen, zur  Errichtung von allgemein zugänglichen Trinkwasserbrunnen an öffentlichen Orten.</p>

            <h2>Brunnen</h2>

            <p>Wir bezeichnen alle auf der Karte angegebenen Trinkwasserstellen als "Brunnen". Vom Brunnen aus Gusseisen, Stein oder Edelstahl bis zu den Wasserhähnen engagierter Cafés haben Sie unzählige Möglichkeiten, Ihre Trinkflasche aufzufüllen!</p>

            <h2>Über 310 000 Wasserstellen in Water-Map.</h2>

            <p>Das Tool listet über 310 000 Trinkwasserbrunnen weltweit auf, mit besonderem Schwerpunkt auf europäischen Ländern, deren Trinkwassersystem streng kontrolliert wird und von hoher Qualität ist.</p>

<h2>Vorsichtsmaßnahmen für die Verwendung</h2>

<p>Water-Map ist kollaborativ. Alle EinwohnerInnen / KaffeehausbetreiberInnen / Gemeinden können die App mit Brunnen / Trinkwasserstellen bereichern. Die Daten (Geolocation und Fotos) stammen von Tausenden von Freiwilligen, die die Brunnen in den Datenbanken von Wiki Data und Open Street Map hinzufügen. Wir können nicht jeden hinzugefügten Brunnen oder Trinkwasserstelle überprüfen. Wir können weder für die mögliche Nichtverfügbarkeit eines Brunnens verantwortlich gemacht werden, noch können wir dessen Qualität garantieren.</p>

            <p>Du musst vorab überprüfen, ob das Wasser trinkbar ist. Trink es nicht, wenn es Verunreinigungen aufweist. Melde bitte allfällige Verunreinigungen den zuständigen lokalen Behörden. Wir empfehlen deine Wasserflasche sauber zu halten. Ein Mangel an Hygiene kann die Entwicklung von Bakterien fördern.</p>

            <h2>Wer wir sind</h2>

            <p>Wir sind Weltenbürger, leben in Frankreich, der Schweiz und anderswo und haben uns zu diesem Gemeinschaftsprojekt zusammengeschlossen. Die Mitglieder des Büros sind alle Freiwillige.</p>

            <p>Die Gründer Stuart und Stephanie Rapoport kämpften mit dem Verein Stop Bottling um ein Projekt zur Abfüllung des Mineralwassers der französischen Stadt Divonne-les-Bains. Das Projekt sah vor, 400 Millionen Flaschen (Plastik) pro Jahr per LKW und Fracht nach Asien zu schicken. Dieser Kampf war erfolgreich, weil das Projekt im September 2019 vom Bürgermeister von Divonne aufgegeben wurde.</p> `,
        },
        tap: {
            label: 'Trinken Sie Leitungswasser',
            title: 'Trinken Sie Leitungswasser',
            page: `<p>Obwohl Leitungswasser nicht überall auf der Welt sicher ist, ist es in vielen Ländern, insbesondere in den Ländern der Europäischen Union, von sehr guter Qualität. Hier sind andere Vorteile:</p>

            <h2>- Kosten</h2>

            <p>Flaschenwasser kostet das 40- bis 200-fache von Leitungswasser.</p>

            <h2>- Weniger Plastik mit Leitungswasser</h2>

            <p>Immer mehr Studien kommen zu dem gleichen Ergebnis. Eine der neuesten stammt aus Kanada und wurde  im Juni 2019 in der Zeitschrift <i>Environmental Science and Technology</i> veröffentlicht. Sie zeigt, dass Erwachsene durch den alleinigen Konsum von Flaschenwasser jährlich bis zu 52.000 Mikropartikel Plastik und 90.000 sonstige Mikropartikel zu sich nehmen. Im Vergleich dazu sind es nur 4000 Mikropartikel beim Konsum von Leitungswasser.</p>

            <h2>- Leitungswasser ist eines der am besten kontrollierten Lebensmittel</h2>

            <p>In allen Mitgliedstaaten der Europäischen Union muss die Wasserversorgung der Verbraucher mindestens 48 Parameter (chemische, mikrobiologische und Indikatoren) erfüllen.</p>

            <p>Das Wasser wird sehr regelmäßig überwacht, um die Sicherheit zu gewährleisten. Die Länder müssen die Ergebnisse dieser Kontrollen veröffentlichen.</p>

            <h2>- Sicheres Quellwasser</h2>

            <p>Vom Konsum von Trinkwasser aus kleineren Wasserversorgungsnetz in dünn besiedelten ländlichen Gebieten wird zum Teil abgeraten. Die Erfüllungsquoten sind jedoch überall sehr hoch: Mit Ausnahme von Ungarn, das knapp darunter lag, erreichten sie in allen Mitgliedstaaten 99%.</p>

            <h2>- Wussten Sie schon?</h2>

            <p>50% des Leitungswassers in den Europe stammt aus Grundwasserquellen, wie dies bei Quellwasser oder Mineralwasser der Fall ist. Die restlichen 50% stammen aus Oberflächengewässern.</p>

            <h2>- Transparenz</h2>

            <p>Sie können die Berichte zu den durchgeführten Prüfungen im Internet einsehen. Alle europäischen Länder veröffentlichen nationale oder regionale Berichte (manchmal sogar zu einzelnen Städten) über die Wasserqualität im Internet.</p>
            `,
        },
        fountain: {
            label: 'Brunnen hinzufügen',
            title: 'Brunnen hinzufügen',
            page: `<p> Water-Map ist eine kollaborative und Open-Data-App. Die Aktualisierung dieser Datenbank hängt von Tausenden von Freiwilligen wie Ihnen ab! </p>



            <h2> Füge ein Foto eines Brunnens hinzu </h2>



            <p> Für Brunnen, die ohne Fotos aufgeführt sind, können Sie jetzt ein Foto hinzufügen! Klicken Sie dazu auf den Link "Foto hinzufügen" im Pop-up, das erscheint, wenn Sie eine Wasserstelle auf der Karte auswählen. </p>



            <h2> Neuen Brunnen hinzufügen </h2>



            <p> Dazu müssen Sie den Standortpunkt des Brunnens in der kollaborativen OpenStreetMap-Datenbank hinzufügen:

            <a href='https://meta.wikimedia.org/wiki/Wikimedia_CH/Project/European_Water_Project/de' target='_blank' rel='noopener'> Zugangsanweisungen </a> </p>



            <h2> Ändern der Eigenschaften eines Brunnens </h2>



            <p> Um den Namen eines Brunnens zu ändern, geben Sie unten an, dass er kein Trinkwasser mehr liefert, oder ändern Sie ein anderes Merkmal:

            <a href='https://meta.wikimedia.org/wiki/Wikimedia_CH/Project/European_Water_Project/de' target='_blank' rel='noopener'> Zugangsanweisungen </a> </p>          `,
        },
    },
};
