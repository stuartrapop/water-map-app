export default {
    translation: {
        camera: {
            closer: 'You must be within 20 meters for the camera to work. ',
            fountainPopupAdd: 'Add/Update image',
            fountainPopupUpdate: 'Update this photo',
            itinerary: 'Itinerary',
            latitude: 'Latitude',
            longitude: 'Longitude',
            success: 'Your photo has been successfully saved.',
            thanks: 'Thanks you for your contribution.',
            cameraPermissionError:
                'You must give Water-Map permission to access your camera',
            labelRetake: 'Re-take',
            labelSave: 'Save Photo',
            labelCancel: 'Cancel',

            saveError: 'Connection problem. Your photo could not be saved.',
            dataError:
                'Connection problem. Please try again when it is restored.',
        },

        bottle: {
            label: 'Adopt a bottle',
            title: 'Adopt a Bottle',
            page: `<h2>Filling your reusable from a drinking fountain is: </h2>

            <p>- staying hydrated easily with high quality water
            - saving money because it's free
            - reducing single use plastic through avoidance</p>

            <h2>Did you know? </h2>

            <p>Any production of plastic or glass bottles has an ecological impact...</p>

             <h2>Plastic bottles </h2>

            <p>To make a one liter plastic bottle, you need 33 ml of oil, 80 g of coal, 42 liters of gas and ... 2 liters of water. </p>

            <h2>Glass bottles </h2>

            <p>According to a study commissioned by Tétra Pack, one-liter bottles of glass induce the emission of 325g of CO2. </p>

            <p>And that's without counting the carbon footprint of truck transport and energy-intensive recycling. </p>

            <h2>Conclusion: the best waste is the one that is not produced!</h2>
            `,
        },
        facebook: {
            label: 'Facebook',
            link: 'href="https://www.facebook.com/EuropeanWaterProject/"',
        },
        plastic: {
            label: 'The plastic problem',
            title: 'The Plastic Problem',
            page: `<h2>Some key figures</h2>

            <p>- One million plastic bottles are purchased every minute worldwide, a figure that is increasing, and about 30% are recycled.

            - 1/3 of plastic waste found in nature, on beaches, are plastic bottles.

            - They pollute the earth, rivers and oceans. They kill 1.5 million animals each year that have ingested microplastics.

            - Making 1 plastic bottle of 1 L requires 100 ml of oil and 2 liters of water.</p>

            <p>If the situation continues, there will be more plastic than fish in the oceans by 2050!</p>


            <h2>Health impacts</h2>

            <p>Many studies show that humans ingest about 5 grams of microplastics per week (they are present in fish, animals, in the water we drink).</p>

            <p>Another aspect that is less thought about: the presence of microplastics in bottled water. It would be on average twice as high as in tap water, if we are to believe the observations of Orb Media with the State University of New York.</p>



            <h2>From manufacturing to waste, bottle plastic is polluting</h2>

            <p>- The plastic in water bottles is made from chemical compounds that can have harmful consequences for our health.
            - Bottles are made of PET plastics that are no longer recyclable after 2 to 3 times (lose their strength qualities).
            - PET can be broken down into small pieces and made into other bottles or textile fibers (blankets, lint), but this is only done in 20% of cases.
            - Most of the waste is therefore buried (in landfills, at 35%) or burned (incinerators 29%). It is said to be "recovered" (the heat released is a source of energy), but this is not virtuous and it generates pollution in the environment again.</p>



            <h2>Is bottled water better than tap water?</h2>

            <p>Brands have convinced many people that bottled water is better than tap water. Yet many blind tests show that tap water is preferred to bottled water (bottled water is so filtered that it no longer tastes good).</p>

            <p>In France, tap water is particularly healthy and highly controlled. And yet, France among the 5 countries in the world that consume the most plastic bottles, behind Mexico, Thailand (which do not have widespread access to drinking water) Italy and Germany. In the case of France, it is indeed mineral water, with sparkling water representing only 18% of our consumption.</p>



            <h2>What can we do?</h2>

            <p>Finally good news: you can act to reduce the extent of this pollution! How? By drinking tap water in every country where it is safe to drink!</p> `,
        },

        project: {
            label: 'About Water-Map',
            title: 'About Water-Map',
            page: `<h2>Created in 2019 by the NGO European Water Project, Water-Map pursues the following goals:</h2>

            <p>- Participate and contribute to the reduction of disposable plastic waste by promoting the consumption of tap water rather than that of bottled water.
            - Encourage the use of Water-Map:  it allows individuals to go to the nearest drinking water points to fill their water bottle for free.
            - Support actions to develop the installation of drinking water fountains in public places so that it is accessible to all.</p>

            <h2>Fountains </h2>

            <p>We call "fountains" all the drinking water points referenced on the map. From the cast iron, stone or stainless steel fountain to the taps of committed cafés, you'll have a thousand and one ways to fill your water bottle!</p>

            <h2>Over 310 000 water points in Water-Map. </h2>

            <p>The tool map lists over 310 000 drinkable water fountains around the world, with a particular focus on European countries whose drinking water system is highly controlled and of high quality.</p>

            <h2>Precautions for use</h2>

            <p>Water-Map is collaborative. Every citizen, coffee owner / community can enrich it by adding fountains / drinking water points. The data (geolocation and photos) come from thousands of volunteers who add the fountains in the databases of Wiki Data and Open Street Map.</p>

            <p>We cannot check every fountain or water point that is added. We cannot be held responsible for the possible unavailability of a fountain, nor can we guarantee the level of quality of its water.</p>

            <p>You must check that the water is drinkable before drinking it, and do not drink it if it has abnormalities. In this case, please report the anomaly to the relevant local authorities.</p>

            <p> We also recommend users to keep their water bottle clean, a lack of hygiene can promote the development of bacteria.</p>

            <h2>Who we are </h2>

            <p>We are citizens of the world, living in France, Switzerland and elsewhere, and who have all come together to work on this collaborative project. The NGO board members are all volunteers. The founders, Stuart and Stephanie Rapoport, fought with the French non-profit (“association”) Stop Embouteillage to stop a project to bottle the mineral water of Divonne-les-Bains, a French town.</p>

            <p>The project planned to send 400 million bottles (plastic) a year by truck and cargo in Asia. This fight was successful because the project was abandoned by the mayor of Divonne in September 2019.</p>
            `,
        },
        tap: {
            label: 'Drink tap water',
            title: 'Drink tap water',
            page: `<p>Although tap water is not safe everywhere in the world, it is of very good quality in many countries, especially those of the European Union. Here are other benefits:</p>

            <h2>- Cost</h2>

            <p>Bottled water costs between 40 to 200 times more than tap water.</p>

            <h2>- Less plastic with tap water</h2>

            <p>More and more studies are coming to the same conclusion. One of the latest is a Canadian study published in the Journal of <i>Environmental Science and Technology</i> in June 2019. It shows that an adult ingests up to 52,000 microparticles of plastic a year, and 90,000 additional microparticles if he drinks only bottled water, compared to only 4,000 from drinking only tap water.</p>

            <h2>- Tap water is one of the most controlled food products</h2>

            <p>In all European Union member countries, water supplied to consumers must comply with a minimum of 48 parameters (chemical, microbiological and other indicators). Water is monitored very regularly, to ensure the safety. Countries are required to publish the results of these tests.</p>

            <p>In some very sparsely populated rural areas, the quality of the water may be unsuitable for consumption. In this case, the information is communicated to the inhabitants, and you must find out from them. But the good news is that compliance rates reach 99% in all EU countries.</p>

            <h2>- Did you know?</h2>

            <p>50% of the tap water in EU Member States comes from groundwater sources, either spring water or mineral water. The remaining 50% comes from surface water such as lakes, rivers etc.</p>

            <h2>- Transparency</h2>

            <p>You can consult on the Internet the reports of the tests that are carried out. All countries in Europe publish national or regional reports (sometimes even by city) on the quality of the water in the network.</p> `,
        },

        fountain: {
            label: 'Add a fountain',
            title: 'Add a fountain',
            page: `<p> Water-Map is a collaborative and Open Data App. The updating of this database depends on thousands of volunteers like you! </p>



            <h2> Add a photo of a fountain </h2>



            <p> For fountains listed without photos, you can now add a photo! To do this, click on the "add a photo" link in the pop-up that appears when you select a water point on the map. </p>



            <h2> Add a new fountain </h2>


            <p> To do this, you need to add the location point of the fountain in the OpenStreetMap collaborative database:

            <a href='https://meta.wikimedia.org/wiki/Wikimedia_CH/Project/European_Water_Project' target='_blank' rel='noopener'> Access Instructions </a> </p>



            <h2> Modifying the characteristics of a fountain </h2>


            <p> Below, to change the name of a fountain, specify that it no longer provides drinking water, or modify any other characteristic:

            <a href='https://meta.wikimedia.org/wiki/Wikimedia_CH/Project/European_Water_Project' target='_blank' rel='noopener'> Access Instructions </a> </p>
            `,
        },
    },
};
