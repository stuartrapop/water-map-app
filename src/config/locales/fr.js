/* eslint-disable no-irregular-whitespace */
export default {
    translation: {
        camera: {
            closer: "Vous devez être à une distance inférieure à 20 m pour que l'appareil photo fonctionne.",
            fountainPopupAdd: 'Ajouter / mettre à jour cette image',
            fountainPopupUpdate: 'Mettre à jour cette photo',
            itinerary: 'Itinéraire',
            latitude: 'Latitude',
            longitude: 'Longitude',
            success: 'Votre photo a bien été enregistrée.',
            thanks: 'Merci pour votre contribution.',
            saveError:
                'Problème de connexion. Votre photo n’a pas pu être enregistrée.',
            dataError:
                'Problème de connexion. Réessayez quand elle sera rétablie.',
        },

        bottle: {
            label: 'Adoptez une gourde ',
            title: 'Adoptez une gourde ',
            page: `<h2>Remplir sa gourde aux fontaines d’eau potable, c’est : </h2>

            <p>- rester hydraté facilement avec de l’eau de qualité
            - faire des économies car c’est gratuit
            - un déchet en plastique à usage unique en moins</p>

            <h2>Le saviez-vous ? </h2>
            <p>Toute production de bouteille en plastique ou en verre a un impact écologique…</p>

            <h2>Les bouteilles en plastique </h2>
            <p>Pour fabriquer une bouteille en plastique d’un litre, il faut 100 ml de pétrole, 80 g de charbon, 42 litres de gaz et… 2 litres d’eau. </p> 


            <h2>Les bouteilles en verre</h2>
            <p>Selon une étude commandée par Tétra Pack, les bouteilles d’un litre en verre induisent l’émission de 325g de CO2.</p>
             

            <p>Et c’est sans compter le bilan carbone du transport en camion et du recyclage énergivore…</p>

             

            <h2>Conclusion : le meilleur déchet est celui que l’on ne produit pas !</h2>

            `,
        },
        facebook: {
            label: 'Facebook',
            link: 'href="https://www.facebook.com/EuropeanWaterProject/"',
        },
        plastic: {
            label: 'Le problème du plastique',
            title: 'Le problème du plastique',
            page: `<h2>Quelques chiffres clés</h2>

<p>- Un million de bouteilles en plastique sont achetées chaque minute dans le monde, chiffre en augmentation, et environ 30% sont recyclées.
- 1/3 des déchets en plastique retrouvés dans la nature, sur les plages, sont des bouteilles en plastique.
- Ils polluent la terre, les rivières et les océans. Ils tuent 1,5 millions d’animaux chaque année qui ont ingéré des microplastiques.
- Fabriquer 1 bouteille en plastique d’1 L nécessite 100 ml de pétrole et 2 litres d’eau.</p>

<p>Si la situation perdure, il y aura plus de plastique que de poissons dans les océans d'ici 2050 !</p>

<h2>Les impacts sur la santé</h2>

<p>De nombreuses études montrent que les humains ingèrent environ 5 grammes de microplastiques par semaine (ils sont présents dans les poissons, les animaux, dans l’eau que nous buvons).</p>

<p>Un autre aspect auquel on pense moins : la présence de microplastiques dans l'eau en bouteille. Elle serait en moyenne deux fois plus élevée que dans l'eau du robinet, si l'on en croit les observations d'Orb Media avec l'Université d'Etat de New York.</p>

<h2>De la fabrication au déchet, le plastique des bouteilles est polluant</h2>

<p>- Le plastique des bouteilles d'eau est fabriqué à partir de composés chimiques qui peuvent avoir des conséquences néfastes sur notre santé.
- Les bouteilles sont composées de plastiques PET qui ne sont plus recyclables au bout de 2 à 3 fois (perdent leurs qualités de résistance).
- On peut réduire le PET en petits morceaux et fabriquer d’autres bouteilles ou fibres textiles (couvertures, peluches), mais c’est seulement fait dans 20% des cas.
- La plupart des déchets sont donc enfouis (dans les décharges, à 35%) ou brûlés (incinérateurs 29%). On dit qu’ils sont « valorisés » (la chaleur dégagée est source d’énergie), mais ce n’est pas vertueux et cela génère de nouveau une pollution dans l’environnement.</p>



<h2>L'eau en bouteille est-elle meilleure que celle du robinet ?</h2>

<p>Les marques ont convaincu beaucoup de monde que l'eau en bouteilles était meilleure que l'eau du robinet. Pourtant, de nombreux tests à l’aveugle montrent que l’eau du robinet est préférée à l’eau en bouteille (les eaux en bouteilles sont tellement filtrées qu’elles n’ont plus de goût).</p>

<p>En France, l’eau du robinet est particulièrement saine et très contrôlée. Et pourtant, la France partie des 5 pays au monde qui consomment le plus de bouteilles en plastique, derrière le Mexique, la Thaïlande (qui n'ont pas largement accès à l'eau potable) l'Italie et l'Allemagne. Dans le cas de la France, il s'agit bien d'eau minérale, l'eau pétillante ne représentant que 18 % de notre consommation.</p>


<h2>Que pouvons-nous faire ?</h2>

<p>Enfin une bonne nouvelle : vous pouvez agir pour réduire l’étendue de cette pollution ! Comment ? En buvant l’eau du robinet dans tous les pays où elle est potable !</p>
             `,
        },

        project: {
            label: 'À propos de Water-Map ',
            title: 'À propos de Water-Map ',
            page: `<h2>Créée par l’ONG Projet Européen de l’Eau, Water-Map poursuit les objectifs suivants : </h2>

            <p>- Participer et contribuer à la réduction des déchets en plastique à usage unique en favorisant la consommation de l’eau du robinet plutôt que celle d’une eau en bouteille.
            - Encourager l’utilisation de Water-Map&nbsp;: cette App localise les points d’eau potable les plus proches de nous pour que nous puissions y remplir nos gourdes gratuitement.
            - Soutenir les actions visant à développer l’installation de fontaines d’eau potable dans lieux publics afin qu’elle soit accessible à tous.</p>

            <h2>Les fontaines</h2>

            <p>Nous appelons « fontaines » tous les points d’eau potables référencés sur la carte. De la fontaine en fonte, en pierre ou en inox à l’eau du robinet de cafés engagés, vous aurez mille et une façons de remplir votre gourde !</p>

            <h2>Plus de 310&nbsp;000 fontaines recensées dans Water-Map</h2>

            <p>La carte-outil recense plus de 310&nbsp;000 fontaines dans le monde entier, même si nous concentrons davantage nos efforts sur les pays d’Europe dont le réseau d’eau potable est très contrôlé et de qualité.</p>

            <h2>Précautions d’utilisation</h2>

            <p>Water-Map est une App collaborative. Chaque citoyen, propriétaire de café / collectivité peut l’enrichir en y ajoutant des fontaines /points d’eau potable. Les données (géolocalisation et photos) émanent des milliers de volontaires qui ajoutent les fontaines dans les bases de données de Wiki Data et d’Open Street Map. Nous ne pouvons pas vérifier chaque fontaine ou point d’eau qui est ajouté. Nous ne pouvons donc pas être tenus responsables de l’indisponibilité éventuelle d’une fontaine, pas plus que nous ne pouvons garantir le niveau de qualité de son eau.</p>


            <p>Vous devez vérifier que l’eau est potable avant de la consommer, et ne pas la boire si elle présente des anormalités. Dans ce cas, merci de reporter l’anomalie auprès des autorités locales compétentes. Nous recommandons également aux utilisateurs de maintenir leur gourde propre, un manque d’hygiène pouvant favoriser le développement de bactéries.</p>

            <h2>Qui sommes-nous</h2>

            <p>Nous sommes des citoyens du monde, vivant en France, en Suisse et ailleurs, et nous nous sommes réunis autour de ce projet collaboratif. Les membres du bureau sont tous bénévoles.</p>

            <p>Les fondateurs, Stuart et Stéphanie Rapoport, ont combattu avec l’association Stop Embouteillage un projet visant à embouteiller l’eau minérale de Divonne-les-Bains, une commune française. Le projet prévoyait d’envoyer 400 millions de bouteilles (en plastique) par an par camion puis cargo en Asie. Cette lutte a été couronnée de succès puisque le projet a été abandonné par la mairie de Divonne en septembre 2019.</p> `,
        },
        tap: {
            label: 'Buvez l’eau du robinet',
            title: 'Buvez l’eau du robinet',
            page: `<p>Si l’eau du robinet n’est pas sûre partout dans le monde, elle est de très bonne qualité dans de nombreux pays, notamment ceux de l’Union Européenne. Voici ses autres avantages :</p>


            <h2>Coût</h2>


            <p>L'eau en bouteille coûte 40 à 200 fois plus cher que l'eau du robinet.</p>

            <h2>- Moins de plastique avec l’eau du robinet</h2>

            <p>De plus en plus d’études le confirment. L’une des dernières en date est une étude canadienne parue dans la revue <i>Environmental Science and Technology</i> en juin 2019. Elle montre qu’un adulte ingère jusqu'à 52.000 microparticules de plastique par an,  et  90 000 microparticules supplémentaires s’il boit uniquement de l’eau en bouteille, contre seulement 4000 s’il se contente de l’eau du robinet.</p>

            <h2>- L’eau du robinet est l’un des aliments les plus contrôlés</h2>

            <p>Dans tous les pays membres de l’Union européenne, l’eau fournie aux consommateurs doit être en conformité avec un minimum de 48 paramètres (chimiques, microbiologiques et indicateurs). L’eau y fait l’objet d’un suivi très régulier, destiné à en garantir la sécurité sanitaire. Les pays sont tenus de publier les résultats de ces contrôles.</p>

            <p>Dans certaines zones rurales très peu denses, la qualité de l’eau peut être impropre à la consommation. Dans ce cas, l’information est communiquée aux habitants, et vous devez vous renseigner auprès d’eux. Mais la bonne nouvelle est que les taux de conformité atteignent 99% dans tous les pays de l’Union européenne.</p>


            <h2>- Le saviez-vous ?</h2>


            50% de l’eau du robinet des États membres de l’U.E provient, comme pour les eaux de sources ou les eaux minérales, de sources souterraines. Les 50% restant proviennent d’eaux de surface.</p>

            <h2>- Transparence</h2>

            <p>Vous pouvez consulter sur Internet les rapports des contrôles qui sont effectués. Tous les pays d’Europe publient des rapports nationaux ou régionaux (parfois même par ville) sur la qualité de l’eau du réseau.</p>
            `,
        },
        fountain: {
            label: 'Ajouter une fontaine',
            title: 'Ajouter une fontaine',
            page: `<p>Water-Map est une App collaborative et Open Data. La mise à jour de cette base de données dépend de milliers de volontaires comme vous !</p>

            <h2>Ajouter la photo d’une fontaine</h2>

            <p>Pour les fontaines recensées sans photos, vous pouvez désormais ajouter une photo ! Pour cela, cliquez sur le lien « ajouter une photo » dans le pop-up qui s’affiche lorsque vous sélectionnez un point d’eau sur la carte.</p>


            <h2> Ajouter une nouvelle fontaine</h2>

            <p>Il faut pour cela ajouter le point de localisation de la fontaine dans la base de données collaborative d'OpenStreetMap :

            <a href='https://meta.wikimedia.org/wiki/Wikimedia_CH/Project/European_Water_Project/fr'  target='_blank' rel='noopener'> Accéder aux Instructions </a> </p>


            <h2> Modifier les caractéristiques d’une fontaine</h2>

            <p>Ci-dessous, comment changer le nom d’une fontaine, spécifier qu’elle ne fournit plus d’eau potable, ou modifier toute autre information :

            <a href='https://meta.wikimedia.org/wiki/Wikimedia_CH/Project/European_Water_Project/fr'  target='_blank' rel='noopener'> Accéder aux Instructions </a> </p>
            `,
        },
    },
};
