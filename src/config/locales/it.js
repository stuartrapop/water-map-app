/* eslint-disable no-irregular-whitespace */
export default {
    translation: {
        camera: {
            closer: 'Affinché la telecamera funzioni, è necessario trovarsi entro 20 metri. ',
            fountainPopupAdd: 'Aggiungi/aggiorna questa immagine',
            fountainPopupUpdate: 'Aggiorna questa foto',
            itinerary: 'Itinerario',
            latitude: 'Latitudine',
            logo: '/images/appLogos/logoEN.png',
            longitude: 'Longitudine',
            success: 'La tua foto è stata salvata.',
            thanks: 'Grazie per il vostro contributo.',
            saveError:
                'Problema di connessione. La tua foto non può essere salvata.',
            dataError:
                'Problema di connessione. Si prega di riprovare quando sarà ripristinato.',
        },

        bottle: {
            label: 'Adotta una borraccia ',
            title: 'Adotta una borraccia ',
            page: `<h2>Riempire la borraccia con fontanelle è: </h2>

            <p>- rimanere facilmente idratato con acqua di qualità
            - risparmiare denaro perché è gratuito
            - rifiuti di plastica monouso in meno</p>

            <h2>Lo sapevi? </h2>

            <p>Qualsiasi produzione di bottiglie di plastica o di vetro ha un impatto ecologico... </p>

            <h2>Bottiglie di plastica </h2>

            <p>Per fare una bottiglia di plastica da un litro, hai bisogno di 100 ml di olio, 80 g di carbone, 42 litri di gas e ... 2 litri di acqua. </p>

            <h2> Bottiglie di vetro </h2>

            <p>Secondo uno studio commissionato da Tétra Pack, le bottiglie di vetro da un litro inducono l'emissione di 325 g di CO2. </p>

            <p>E questo senza contare l'impronta di carbonio del trasporto di camion e del riciclaggio ad alta intensità energetica... </p>

            <h2>Conclusione: lo spreco migliore è quello che non viene prodotto!</h2>
            `,
        },
        facebook: {
            label: 'Facebook',
            link: 'href="https://www.facebook.com/EuropeanWaterProject/"',
        },
        plastic: {
            label: 'Il problema della plastica ',
            title: 'Il problema della plastica ',
            page: `<h2>Alcune cifre chiave</h2>

            <p>- Un milione di bottiglie di plastica vengono acquistate ogni minuto nel mondo, una cifra che sta aumentando, e circa il 30% viene riciclato.
            - 1/3 dei rifiuti di plastica trovati in natura, sulle spiagge, sono bottiglie di plastica.
            - Inquinano la terra, i fiumi e gli oceani. Uccidono ogni anno 1,5 milioni di animali che hanno ingerito microplastiche.
            - Per fare 1 L di bottiglia di plastica ci vogliono 100 ml di olio e 2 litri di acqua. </p>

            <p> Se la situazione continua, ci sarà più plastica che pesce negli oceani entro il 2050! </p>


            <h2>Impatti sulla salute</h2>

            <p>Molti studi mostrano che gli esseri umani ingeriscono circa 5 grammi di microplastiche a settimana (sono presenti nei pesci, negli animali, nell'acqua che beviamo).</p>

            <p>Un altro aspetto a cui si pensa meno: la presenza di microplastiche nell'acqua in bottiglia. Si dice che sia in media due volte più alta che nell'acqua di rubinetto, se le osservazioni di Orb Media con l'Università Statale di New York sono da credere. La fabbricazione dell'acqua è un fattore chiave nello sviluppo di nuovi prodotti.</p>


            <h2>Dalla produzione ai rifiuti, la plastica nelle bottiglie è inquinante</h2>

            <p>- La plastica delle bottiglie d'acqua è fatta di composti chimici che possono avere conseguenze dannose per la nostra salute.
            - Le bottiglie sono fatte di plastica PET che non sono più riciclabili dopo 2 o 3 volte (perdono le loro qualità di resistenza).
            - Il PET può essere scomposto in pezzi più piccoli e trasformato in altre bottiglie o fibre tessili (coperte, lanugine), ma questo viene fatto solo nel 20% dei casi.
            - La maggior parte dei rifiuti è quindi sepolta (nelle discariche, al 35%) o bruciata (inceneritori 29%). Si dice che viene "recuperato" (calore rilasciato = fonte di energia), ma questo non è virtuoso e genera di nuovo inquinamento nell'ambiente.</p>

            <h2>L'acqua in bottiglia è migliore di quella del rubinetto?</h2>

            <p>I marchi hanno convinto molte persone che l'acqua in bottiglia è meglio di quella del rubinetto. Eppure numerosi test alla cieca dimostrano che l'acqua del rubinetto è preferita all'acqua in bottiglia (l'acqua in bottiglia è così filtrata che non ha più un buon sapore).</p>

            <p>In Francia, l'acqua del rubinetto è particolarmente sana e molto controllata. Eppure, la Francia è tra i 5 paesi al mondo che consumano più bottiglie di plastica, dietro il Messico, la Thailandia (che non hanno un accesso diffuso all'acqua potabile) l'Italia e la Germania. Nel caso della Francia, si tratta effettivamente di acqua minerale, con l'acqua frizzante che rappresenta solo il 18% del nostro consumo.</p>

            <h2>Che cosa possiamo fare?</h2>

            <p>Finalmente una buona notizia: puoi agire per ridurre la portata di questo inquinamento! Come? Bevendo l'acqua del rubinetto in tutti i paesi dove è sicura da bere! La buona notizia è che puoi fare qualcosa per ridurre la portata di questo inquinamento!</p>
             `,
        },

        project: {
            label: 'Informazioni su Water-Map',
            title: 'Informazioni su Water-Map',
            page: `<h2> Creato nel 2019 dalla ONG European Water Project, Water-Map persegue i seguenti obiettivi: </h2>

            <p>- Partecipare e contribuire alla riduzione dei rifiuti di plastica usa e getta promuovendo il consumo di acqua di rubinetto piuttosto che quello di acqua in bottiglia.
            - Incoraggiare l'uso di Water-Map che consente alle persone di recarsi nei punti di acqua potabile più vicini per riempire la zucca gratuitamente.
            - Supportare le azioni per sviluppare l'installazione di fontanelle per l'acqua potabile in luoghi pubblici in modo che sia accessibile a tutti.</p>

            <h2>Fontane</h2>

            <p>Chiamiamo "fontane" tutti i punti di acqua potabile indicati sulla mappa. Dalla fontana in ghisa, pietra o acciaio inossidabile ai rubinetti dei caffè impegnati, avrai mille e un modo per riempire la tua bottiglia d'acqua! <br>

            <h2>Oltre 310 000 punti d'acqua in Water Map.</h2>

            <p>Lo strumento mappa elenca oltre 310 000 fontane d'acqua potabile in tutto il mondo, con particolare attenzione ai paesi europei il cui sistema di acqua potabile è altamente controllato e di alta qualità.</p>

            <h2>Precauzioni per l'uso</h2>

            <p>Water-Map è collaborativa. Ogni cittadino, proprietario / comunità del caffè può arricchirlo aggiungendo fontane / punti d'acqua potabile. I dati (geolocalizzazione e foto) provengono da migliaia di volontari che aggiungono le fontane nei database di Wiki Data e Open Street Map. Non possiamo controllare ogni fontana o punto d'acqua aggiunto. Non possiamo essere ritenuti responsabili per l'eventuale indisponibilità di una fontana, né possiamo garantire il livello di qualità della sua acqua.</p>

            <p>È necessario verificare che l'acqua sia potabile prima di mangiarla e non berla se presenta anomalie. In questo caso, si prega di segnalare l'anomalia alle autorità locali competenti. Raccomandiamo inoltre agli utenti di mantenere pulita la loro bottiglia d'acqua, una mancanza di igiene che può favorire lo sviluppo di batteri.</p>

            <h2> Chi siamo</h2>

            <p>Siamo cittadini del mondo, viviamo in Francia, Svizzera e altrove e ci siamo riuniti attorno a questo progetto di collaborazione. I membri dell'ufficio sono tutti volontari.</p>

            <p>I fondatori, Stuart e Stephanie Rapoport, hanno combattuto con l'associazione Stop Bottling un progetto per imbottigliare l'acqua minerale di Divonne-les-Bains, una città francese. Il progetto prevedeva di inviare 400 milioni di bottiglie (di plastica) all'anno in camion e merci in Asia. Questa lotta ha avuto successo perché il progetto è stato abbandonato dal sindaco di Divonne a settembre 2019.</p>
            `,
        },
        tap: {
            label: 'Bevi acqua di rubinetto',
            title: 'Bevi acqua di rubinetto',
            page: `<p>Sebbene l'acqua del rubinetto non sia sicura in qualsiasi parte del mondo, è di ottima qualità in molti paesi, in particolare quelli dell'Unione Europea. Ecco altri vantaggi:</p>

            <h2>- Costo</h2>

            <p>L'acqua in bottiglia costa da 40 a 200 volte più dell'acqua di rubinetto.</p>

            <h2>- Meno plastica con acqua di rubinetto</h2>

            <p>Sempre più studi lo hanno aggiornato. Uno degli ultimi è uno studio canadese pubblicato sulla rivista <i>Environmental Science and Technology</i> a giugno 2019. Mostra che un adulto ingerisce fino a 52.000 microparticelle di plastica all'anno e 90.000 microparticelle aggiuntive se beve solo acqua in bottiglia, rispetto a solo 4000 se è soddisfatta con acqua di rubinetto.</p>

            <h2>- L'acqua del rubinetto è uno degli alimenti più controllati</h2>

            <p>In tutti i paesi membri dell'Unione Europea, l'acqua fornita ai consumatori deve rispettare un minimo di 48 parametri (chimici, microbiologici e indicatori).</p>

            <p>L'acqua viene monitorata molto regolarmente, per garantire la sicurezza. I paesi sono tenuti a pubblicare i risultati di questi controlli.</p>

            <h2>- Safe Spring Water</h2>

            <p>L'acqua distribuita da piccole reti a volte può essere scoraggiata in aree rurali scarsamente popolate in cui bisogna sempre indagare. Ma i tassi di conformità in tutto il mondo sono molto elevati: hanno raggiunto il 99% in tutti gli Stati membri, ad eccezione dell'Ungheria, che era appena inferiore.</p>

            <h2>Lo sapevi?</h2>

            <p>Il 50% dell'acqua del rubinetto negli Stati membri degli Stati Uniti proviene da fonti sotterranee, come nel caso dell'acqua di sorgente o di acqua minerale. Il restante 50% proviene da acque superficiali.</p>

            <h2>- Trasparenza</h2>

            <p>È possibile consultare su Internet i rapporti dei controlli effettuati. Tutti i paesi in Europa pubblicano relazioni nazionali o regionali (a volte anche per città) sulla qualità dell'acqua nella rete.</p>
            `,
        },

        fountain: {
            label: 'Aggiungi una fontana ',
            title: 'Aggiungi una fontana ',
            page: `<p> Water-Map è un'app collaborativa e Open Data. L'aggiornamento di questo database dipende da migliaia di volontari come te! </p>



            <h2> Aggiungi una foto di una fontana </h2>



            <p> Per le fontane elencate senza foto, ora puoi aggiungere una foto! Per fare ciò, fai clic sul link "aggiungi una foto" nel pop-up che appare quando selezioni un punto d'acqua sulla mappa. </p>



            <h2> Aggiungi una nuova fontana </h2>



            <p> Per fare ciò, è necessario aggiungere il punto di ubicazione della fontana nel database collaborativo di OpenStreetMap:

            <a href='https://meta.wikimedia.org/wiki/Wikimedia_CH/Project/European_Water_Project/it' target='_blank' rel='noopener'> Istruzioni per l'accesso </a> </p>



            <h2> Modifica delle caratteristiche di una fontana </h2>



            <p> Di seguito, per cambiare il nome di una fontana, specificare che non fornisce più acqua potabile, o modificare qualsiasi altra caratteristica:


            <a href='https://meta.wikimedia.org/wiki/Wikimedia_CH/Project/European_Water_Project/it' target='_blank' rel='noopener'> Istruzioni per l'accesso </a> </p>
                       `,
        },
    },
};
