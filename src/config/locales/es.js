/* eslint-disable no-irregular-whitespace */
export default {
    translation: {
        camera: {
            closer: 'Debes estar a menos de 20 metros para que la cámara funcione. ',
            fountainPopupAdd: 'Añadir/Actualizar esta imagen',
            fountainPopupUpdate: 'Actualiza esta foto',
            itinerary: 'Itinerario',
            latitude: 'Latitud',
            longitude: 'Longitude',
            success: 'Tu foto ha sido guardada.',
            thanks: 'Gracias por su contribución.',
            saveError: 'Problema de conexión. Tu foto no se ha podido guardar.',
            dataError:
                'Problema de conexión. Por favor, inténtelo de nuevo cuando se restablezca.',
        },

        bottle: {
            label: 'Adopta una botella ',
            title: 'Adopta una botella ',
            page: `<h2>Rellenar su botella sostenible en fuentes de agua potable es: </h2>

            <p>- mantenerse hidratado fácilmente con agua de calidad
            - ahorrar dinero porque es gratis
            - reducir el uso único de botellas de plástico</p>

            <h2>¿Lo sabías? </h2>

            <p>Cualquier producción de botellas de plástico o vidrio tiene un impacto ecológico... </p>

            <h2>Botellas de plástico </h2>

            <p>Para hacer una botella de plástico de un litro se necesitan: 100 ml de aceite, 80 g de carbón, 42 litros de gas y ... 2 litros de agua. </p>

            <h2>Botellas de vidrio </h2>

            <p>Según un estudio encargado por Tétra Pack, las botellas de vidrio de un litro contribuyen a la emisión de 325 g de CO2. </p>

            <p>Y eso sin contar con la huella de carbono que dejan, tanto el transporte de camiones, como el consumo intensivo de energía que requiere reciclar...</p>

            <h2>Conclusión: ¡el mejor desperdicio es el que no se produce!</h2>
            `,
        },
        facebook: {
            label: 'Facebook',
            link: 'href="https://www.facebook.com/EuropeanWaterProject/"',
        },
        plastic: {
            label: 'El problema del plástico',
            title: 'El problema del plástico',
            page: `<h2>Algunas cifras clave</h2>

            <p>- Cada minuto se compran un millón de botellas de plástico en todo el mundo, una cifra que va en aumento, y alrededor del 30% se reciclan.
            - 1/3 de los residuos de plástico que se encuentran en la naturaleza, en las playas, son botellas de plástico.
            - Contaminan la tierra, los ríos y los océanos. Cada año mueren 1,5 millones de animales que han ingerido microplásticos.
            - Para fabricar una botella de plástico de 1 L se necesitan 100 ml de aceite y 2 litros de agua.</p>

            <p>Si la situación continúa, en 2050 habrá más plástico que peces en los océanos.</p>



            <h2>Impacto en la salud</h2>

            <p>Muchos estudios demuestran que el ser humano ingiere unos 5 gramos de microplásticos a la semana (están presentes en los peces, en los animales, en el agua que bebemos).</p>

            <p>Otro aspecto en el que se piensa menos: la presencia de microplásticos en el agua embotellada. Se dice que la media es dos veces más alta que en el agua del grifo, si las observaciones de Orb Media con la Universidad Estatal de Nueva York son creíbles.</p>



            <h2>Desde la fabricación hasta los residuos, el plástico de las botellas es contaminante</h2>

            <p>- El plástico de las botellas de agua está fabricado con compuestos químicos que pueden tener consecuencias perjudiciales para nuestra salud.
            - Las botellas están fabricadas con plásticos PET que dejan de ser reciclables después de 2 o 3 veces (pierden sus cualidades de resistencia).
            - El PET puede descomponerse en trozos más pequeños y convertirse en otras botellas o fibras textiles (mantas, pelusas), pero esto sólo se hace en el 20% de los casos.
            - Por ello, la mayor parte de los residuos se entierra (en vertederos, con un 35%) o se quema (incineradoras, con un 29%). Se dice que se "recupera" (el calor liberado es una fuente de energía), pero esto no es virtuoso y vuelve a generar contaminación en el medio ambiente.</p>



            <h2>¿Es mejor el agua embotellada que el agua del grifo?</h2>

            <p>Las marcas han convencido a mucha gente de que el agua embotellada es mejor que la del grifo. Sin embargo, numerosas pruebas ciegas demuestran que se prefiere el agua del grifo al agua embotellada (el agua embotellada está tan filtrada que ya no sabe bien).</p>

            <p>En Francia, el agua del grifo es especialmente saludable y está muy controlada. Y sin embargo, Francia se encuentra entre los 5 países del mundo que más botellas de plástico consumen, por detrás de México, Tailandia (que no tiene acceso generalizado al agua potable) Italia y Alemania. En el caso de Francia, se trata efectivamente de agua mineral, ya que el agua con gas sólo representa el 18% de nuestro consumo.</p>



            <h2>¿Qué podemos hacer?</h2>

            <p>Por fin una buena noticia: ¡puedes actuar para reducir el alcance de esta contaminación! ¿Cómo? Bebiendo agua del grifo en todos los países en los que se puede beber! </p>
             `,
        },

        project: {
            label: 'Acerca de Water-Map',
            title: 'Acerca de Water-Map',
            page: `<h2> Creado en 2019 por la ONG European Water Project, Water-Map persigue los siguientes objetivos:  </h2>

            <p>- Participar y contribuir a la reducción de los desechos plásticos promoviendo el consumo de agua del grifo en lugar de agua embotellada.
            - Fomentar el uso de Water-Map que permita a las personas ir a los puntos de agua potable más cercanos para llenar su botella de agua de forma gratuita.
            - Apoyar acciones para promover la instalación de fuentes de agua potable en lugares públicos para que sea accesible para todos. </p>

            <h2>Fuentes</h2>

            <p>Llamamos "fuentes" a todos los puntos de agua potable a los que se hace referencia en el mapa. Desde fuentes de hierro fundido, de piedra o de acero inoxidable hasta los grifos de cafés comprometidos con la causa, ¡tendrás mil y una formas de llenar tu botella de agua!</p>

            <h2>Más de 310 000 puntos de agua en Water-Map.</h2>

            <p>El mapa de la herramienta enumera más de 310 000 fuentes de agua potable en todo el mundo, con especial atención a los países europeos cuyo sistema de agua potable está muy controlado y es de alta calidad.</p>

            <h2>Precauciones de uso</h2>

            <p>Water-Map es pública. Cada ciudadano, propietario de cafeterías / y otras comunidades puede enriquecerlo agregando fuentes / puntos de agua potable. Los datos (geolocalización y fotos) provienen de miles de voluntarios que agregan las fuentes en las bases de datos de Wiki Data y Open Street Map. No podemos verificar cada fuente o punto de agua que se agrega. No podemos responsabilizarnos por la posible falta de disponibilidad de una fuente, ni podemos garantizar el nivel de calidad de su agua.</p>

            <p>Debe verificar que el agua sea potable antes de beberla, y no la tome si tiene anormalidades. En cuyo caso, informe de la anomalía a las autoridades locales pertinentes. También recomendamos a los usuarios que mantengan limpia su botella de agua, la falta de higiene puede promover el desarrollo de bacterias.</p>

            <h2>Quiénes somos</h2>

            <p>Somos ciudadanos del mundo, vivimos en Francia, Suiza y otros lugares, que decidimos reunirnos en torno a este proyecto de colaboración. Los Miembros del Consejo y Dirección de la ONG somos todos voluntarios.</p>

            <p>Los fundadores, Stuart y Stephanie Rapoport, lucharon con la ONG Stop Bottling en un proyecto para embotellar el agua mineral de Divonne-les-Bains, un pueblo francés. El proyecto planeaba enviar 400 millones de botellas (de plástico) al año por camión y barco a Asia. La lucha fue un éxito y el proyecto fue abandonado por el alcalde de Divonne en septiembre de 2019.</p>
            `,
        },
        tap: {
            label: 'Beber agua del grifo',
            title: 'Beber agua del grifo',
            page: `<p>Aunque el agua del grifo no es segura en todo el mundo, es de muy buena calidad en muchos países, especialmente en los de la Unión Europea. Aquí hay otros beneficios:</p>

            <h2>- Costo</h2>

            <p>El agua embotellada cuesta 40 a 200 veces más que el agua del grifo.</p>

            <h2>- Menos plástico con agua corriente</h2>

            <p>Más y más estudios lo han confirmado. Uno de los últimos es un estudio canadiense publicado en la revista <i>Environmental Science and Technology</i> en junio de 2019. Muestra que un adulto ingiere hasta 52,000 micropartículas de plástico al año y 90,000 micropartículas adicionales si solo bebe agua embotellada, en comparación con solo 4,000 si consume solamente agua del grifo.</p>

            <h2>- El agua del grifo es uno de los alimentos más controlados</h2>

            <p>En todos los países miembros de la Unión Europea, el agua suministrada a los consumidores debe cumplir con un mínimo de 48 parámetros (químicos, microbiológicos e indicadores).</p>

            <p>El agua se controla con mucha frecuencia para garantizar su calidad. Cada país debe publicar los resultados de estos controles.</p>

            <h2>- Agua de manantial segura</h2>

            <p>El agua distribuida en áreas rurales con población escasa y dispersa, el agua puede no ser apta para el consumo. En dichos casos, la información es transmitida a los habitantes afectados. En esos casos, hay que preguntar a la gente local para que confirmen si el agua local es potable o no. La buena noticia, es que la tasa de cumplimiento de agua de calidad es del 99% en todos los Estados miembros, excepto Hungría, que estaba justo por debajo.</p>

            <h2>- ¿Lo sabía?</h2>

            <p>El 50% del agua del grifo en la Unión Europea proviene de fuentes de agua subterránea, como es el caso del agua de manantial o agua mineral. El 50% restante proviene de aguas superficiales, como ríos, lagos, etc.</p>

            <h2>- Transparencia</h2>

            <p>Puede consultar en Internet los informes de los controles que se realizan. Todos los países Europeos publican informes a nivel nacional o regional (a veces incluso por ciudad) sobre la calidad del agua en la red.</p>
            `,
        },
        fountain: {
            label: 'Agregar una fuente',
            title: 'Agregar una fuente',
            page: `<p> Water-Map es una aplicación colaborativa y de datos abiertos. ¡La actualización de esta base de datos depende de miles de voluntarios como tú! </p>



            <h2> Agrega una foto de una fuente </h2>



            <p> Para las fuentes enumeradas sin fotos, ¡ahora puede agregar una foto! Para hacer esto, haga clic en el enlace "agregar una foto" en la ventana emergente que aparece cuando selecciona un punto de agua en el mapa. </p>



            <h2> Agregar una nueva fuente </h2>



            <p> Para hacer esto, debe agregar el punto de ubicación de la fuente en la base de datos colaborativa de OpenStreetMap:

            <a href='https://meta.wikimedia.org/wiki/Wikimedia_CH/Project/European_Water_Project/es' target='_blank' rel='noopener'> Instrucciones de acceso </a> </p>



            <h2> Modificar las características de una fuente </h2>



            <p> A continuación, para cambiar el nombre de una fuente, especifique que ya no proporciona agua potable, o modifique cualquier otra característica:
            <a href='https://meta.wikimedia.org/wiki/Wikimedia_CH/Project/European_Water_Project/es' target='_blank' rel='noopener'> Instrucciones de acceso </a> </p>
                       `,
        },
    },
};
