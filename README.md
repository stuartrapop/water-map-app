### Project Overview

Water-Map APP
To promote the use of water bottles that can be refilled in a sustainable way. This App is a collaborative project developed in partnership with OpenStreetMap.


Over 310,000 drinkable water points can be found worldwide on the app. With Water-Map, you can find a place to refill your water bottle anywhere for free. Together let's refill, reuse and reduce the consumption of single-use plastic!

### Other instructions

rename appTemp.json to app.json and add googlemap keys and unique ids if pushing to app store

##### Special Thanks - 
“This project is tested with BrowserStack.”

##### Project License - 
MIT License - please link back to the project if you like it.
